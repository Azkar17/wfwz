<?php

use App\Mail\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AddonController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\HomepageController;


//Homepage
Route::get('/',[HomepageController::class,'homepage'])->name('home');
Route::get('/azkar',[HomepageController::class,'azkar']);
// Route::get('/package',[HomepageController::class,'listpackage'])->name('public.package');
Route::get('/package/booking',[HomepageController::class,'singlepackage'])->name('single');
Route::get('/package/double/booking',[HomepageController::class,'doublepackage'])->name('double');
Route::get('/package/kapas/booking',[HomepageController::class,'kapaspackage'])->name('kapas');
Route::post('/book',[HomepageController::class,'homapagebook'])->name('public.book');
Route::get('/register', function () {
    return view('dashboard.register');
});
Route::get('/gallery', function () {
    return view('homepage.gallery');
});
// Route::group(['middleware' => ['auth']], function () {
//package
Route::get('/manage/package',[PackageController::class, 'index'])->name('package');
Route::get('/package/{package}/edit',[PackageController::class, 'edit'])->name('package.edit');
Route::get('/package/{package}/delete',[PackageController::class, 'destroy'])->name('package.delete');
Route::post('/store/package',[PackageController::class, 'store'])->name('store.package');
Route::post('/update/package',[PackageController::class, 'update'])->name('update.package');

// //Add on
// Route::get('/addons',[AddonController::class,'index'])->name('addon');
// Route::post('/store/addon',[AddonController::class, 'store'])->name('store.addons');
// Route::post('/update/addon',[AddonController::class, 'update'])->name('update.addons');
// Route::get('/addon/{addon}/edit',[AddonController::class, 'edit'])->name('addon.edit');
// Route::get('/addon/{addon}/delete',[AddonController::class, 'destroy'])->name('addon.delete');


//booking 
Route::get('/booking',[BookingController::class,'index'])->name('booking');
Route::post('/book/package',[BookingController::class,'store'])->name('booking.store');
Route::get('/booking/{booking}/edit',[BookingController::class, 'edit'])->name('booking.edit');
Route::post('/booking/update',[BookingController::class, 'update'])->name('booking.update');
Route::get('/booking/{booking}/delete',[BookingController::class, 'destroy'])->name('booking.delete');
Route::get('/booking/
',[BookingController::class ,'filterbypackage'])->name('filter.package');
Route::get('/booking/name',[BookingController::class ,'filterbyname'])->name('filter.name');
Route::get('/booking/date',[BookingController::class ,'filterdate'])->name('filter.date');

// });

//register 
// Route::get('/login',[AuthController::class,'index'])->name('login');
// Route::post('/login',[AuthController::class,'login'])->name('login');
// Route::post('/register/post',[AuthController::class,'register'])->name('register');
// Route::post('/register',[AuthController::class,'registerpage']);
// Route::get('/logout',[AuthController::class,'logout'])->name('logout');
//package return json
Route::get('publicpackage/{package}',[PackageController::class, 'getpackage'])->name('getpackage');

Route::get('packagenew',[HomepageController::class,'listpackage'])->name('public.package');
Route::get('publicbooking',function(){return view('homepage.bookingpackage');});
Route::get('cancelbook',[HomepageController::class,'cancelBookingPage'])->name('cancelepage');
Route::post('store/cancelbook',[HomepageController::class,'StoreCancelBooking'])->name('save.cancelbook');

// Route::get('toh',function(Request $request){
//     // dd($request->all());
//     return view('homepage.bookingpackage',compact('request'));
// })->name('ini');


// Route::get('loopers',function(){
//     for($i=0;$i<200;$i++){
//         echo 'ini number '.$i.'<br>';
//     }
// });

Route::get('nice',
function(){
    Mail::to("azkarneedforspeed@gmail.com")->send(new Test());
    return 'done'; 
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
