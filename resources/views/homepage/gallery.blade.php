@extends('base.landing')
@section('content')
		<div id="wrap">
            @include('base.header')

			<main id="content" class="content">

                <section class="vc_row pt-120 pb-120">
                    <div class="container">
                        <div class="row">
                            <h2 class="text-underline text-center">Gallery</h2>
        
                            <div class="lqd-column col-md-12">
        
                                <div class="liquid-portfolio-list">
        
                                        <div
                                            class="row liquid-portfolio-list-row"
                                            data-columns="3"
                                            data-liquid-masonry="true"
                                            data-masonry-options='{ "layoutMode": "masonry", "alignMid": true }'
                                            data-custom-animations="true"
                                            data-ca-options='{"triggerHandler":"inview","animationTarget":".ld-pf-item","animateTargetsWhenVisible":"true","duration":"1400","delay":"180","easing":"easeOutQuint","initValues":{"translateY":75,"scale":0.75,"opacity":0},"animations":{"translateY":0,"scale":1,"opacity":1}}'
                                        >
                                            
                                            <div class="lqd-column col-md-4 col-sm-6 col-xs-12 grid-stamp creative-masonry-grid-stamp"></div>
                                    
                                            <div class="lqd-column col-md-4 col-sm-6 col-xs-12 masonry-item">
                                                
                                                <div class="ld-pf-item ld-pf-light pf-details-inside pf-details-full pf-details-h-mid pf-details-v-mid title-size-32 ld-pf-semiround">
                                                    
                                                    <div class="ld-pf-inner">
                                                        
                                                        <div class="ld-pf-image">
                                                            <figure style="background-image: url({{asset('gambar/1.jpg')}});">
                                                                <img src="{{asset('gambar/1.jpg')}}" alt="Portfolio Image">
                                                            </figure>
                                                        </div><!-- /.ld-pf-image -->
                                    
                                                        <div class="ld-pf-bg bg-gradient-primary-bl opacity-08"></div><!-- /.ld-pf-bg -->
                                                        
                                                        <div class="ld-pf-details" data-custom-animations="true" data-ca-options='{ "triggerHandler": "mouseenter", "triggerTarget": ".ld-pf-item", "triggerRelation": "closest", "offTriggerHandler": "mouseleave", "animationTarget": ".split-inner", "duration": 200, "delay": 35, "offDuration": 100, "easing": "easeOutCubic", "initValues": { "translateX": 50, "rotateZ": -75, "opacity": 0, "transformOrigin": [0, "-100%", 0] }, "animations": { "translateX": 0, "rotateZ": 0, "opacity": 1, "transformOrigin": [0, "0%", 0] } }'>
                                                            <div class="ld-pf-details-inner">
                                                                <h3 class="ld-pf-title h4 font-weight-semibold" data-split-text="true" data-split-options='{ "type": "chars" }'>Discover</h3>
                                                            </div><!-- /.ld-pf-details-inner -->
                                                        </div><!-- /.ld-pf-details -->
                                                        
                                                        <a href="#" class="liquid-overlay-link"></a>
                                                        
                                                    </div><!-- /.ld-pf-inner -->
                                                    
                                                </div><!-- /.ld-pf-item -->
                                                
                                            </div><!-- /.col-lg-4 col-md-6 -->
                                    
                                            <div class="lqd-column col-md-4 col-sm-6 col-xs-12 masonry-item">
                                                
                                                <div class="ld-pf-item ld-pf-light pf-details-inside pf-details-full pf-details-h-mid pf-details-v-mid title-size-32 ld-pf-semiround">
                                                    
                                                    <div class="ld-pf-inner">
                                                        
                                                        <div class="ld-pf-image">
                                                            <figure style="background-image: url({{asset('gambar/2.jpg')}});">
                                                                <img src="{{asset('gambar/2.jpg')}}" alt="Portfolio Image">
                                                            </figure>
                                                        </div><!-- /.ld-pf-image -->
                                    
                                                        <div class="ld-pf-bg bg-gradient-primary-bl opacity-08"></div><!-- /.ld-pf-bg -->
                                                        
                                                        <div class="ld-pf-details" data-custom-animations="true" data-ca-options='{ "triggerHandler": "mouseenter", "triggerTarget": ".ld-pf-item", "triggerRelation": "closest", "offTriggerHandler": "mouseleave", "animationTarget": ".split-inner", "duration": 200, "delay": 35, "offDuration": 100, "easing": "easeOutCubic", "initValues": { "translateX": 50, "rotateZ": -75, "opacity": 0, "transformOrigin": [0, "-100%", 0] }, "animations": { "translateX": 0, "rotateZ": 0, "opacity": 1, "transformOrigin": [0, "0%", 0] } }'>
                                                            <div class="ld-pf-details-inner">
                                                                <h3 class="ld-pf-title h4 font-weight-semibold" data-split-text="true" data-split-options='{ "type": "chars" }'>Discover</h3>
                                                            </div><!-- /.ld-pf-details-inner -->
                                                        </div><!-- /.ld-pf-details -->
                                                        
                                                        <a href="#" class="liquid-overlay-link"></a>
                                                        
                                                    </div><!-- /.ld-pf-inner -->
                                                    
                                                </div><!-- /.ld-pf-item -->
                                                
                                            </div><!-- /.col-lg-4 col-md-6 -->
                                            
                                            <div class="lqd-column col-md-4 col-sm-6 col-xs-12 masonry-item">
                                                
                                                <div class="ld-pf-item ld-pf-light pf-details-inside pf-details-full pf-details-h-mid pf-details-v-mid title-size-32 ld-pf-semiround">
                                                    
                                                    <div class="ld-pf-inner">
                                                        
                                                        <div class="ld-pf-image">
                                                            <figure style="background-image: url('{{asset('gambar/3.jpg')}}');">
                                                                <img src="{{asset('gambar/3.jpg')}}" alt="Portfolio Image">
                                                            </figure>
                                                        </div><!-- /.ld-pf-image -->
                                    
                                                        <div class="ld-pf-bg bg-gradient-primary-bl opacity-08"></div><!-- /.ld-pf-bg -->
                                                        
                                                        <div class="ld-pf-details" data-custom-animations="true" data-ca-options='{ "triggerHandler": "mouseenter", "triggerTarget": ".ld-pf-item", "triggerRelation": "closest", "offTriggerHandler": "mouseleave", "animationTarget": ".split-inner", "duration": 200, "delay": 35, "offDuration": 100, "easing": "easeOutCubic", "initValues": { "translateX": 50, "rotateZ": -75, "opacity": 0, "transformOrigin": [0, "-100%", 0] }, "animations": { "translateX": 0, "rotateZ": 0, "opacity": 1, "transformOrigin": [0, "0%", 0] } }'>
                                                            <div class="ld-pf-details-inner">
                                                                <h3 class="ld-pf-title h4 font-weight-semibold" data-split-text="true" data-split-options='{ "type": "chars" }'>Discover</h3>
                                                            </div><!-- /.ld-pf-details-inner -->
                                                        </div><!-- /.ld-pf-details -->
                                                        
                                                        <a href="#" class="liquid-overlay-link"></a>
                                                        
                                                    </div><!-- /.ld-pf-inner -->
                                                    
                                                </div><!-- /.ld-pf-item -->
                                                
                                            </div><!-- /.col-lg-4 col-md-6 -->
                                            
                                            <div class="lqd-column col-md-4 col-sm-6 col-xs-12 masonry-item">
                                                
                                                <div class="ld-pf-item ld-pf-light pf-details-inside pf-details-full pf-details-h-mid pf-details-v-mid title-size-32 ld-pf-semiround">
                                                    
                                                    <div class="ld-pf-inner">
                                                        
                                                        <div class="ld-pf-image">
                                                            <figure style="background-image: url('{{asset('gambar/4.jpg')}}');">
                                                                <img src="{{asset('gambar/4.jpg')}}" alt="Portfolio Image">
                                                            </figure>
                                                        </div><!-- /.ld-pf-image -->
                                    
                                                        <div class="ld-pf-bg bg-gradient-primary-bl opacity-08"></div><!-- /.ld-pf-bg -->
                                                        
                                                        <div class="ld-pf-details" data-custom-animations="true" data-ca-options='{ "triggerHandler": "mouseenter", "triggerTarget": ".ld-pf-item", "triggerRelation": "closest", "offTriggerHandler": "mouseleave", "animationTarget": ".split-inner", "duration": 200, "delay": 35, "offDuration": 100, "easing": "easeOutCubic", "initValues": { "translateX": 50, "rotateZ": -75, "opacity": 0, "transformOrigin": [0, "-100%", 0] }, "animations": { "translateX": 0, "rotateZ": 0, "opacity": 1, "transformOrigin": [0, "0%", 0] } }'>
                                                            <div class="ld-pf-details-inner">
                                                                <h3 class="ld-pf-title h4 font-weight-semibold" data-split-text="true" data-split-options='{ "type": "chars" }'>Discover</h3>
                                                            </div><!-- /.ld-pf-details-inner -->
                                                        </div><!-- /.ld-pf-details -->
                                                        
                                                        <a href="#" class="liquid-overlay-link"></a>
                                                        
                                                    </div><!-- /.ld-pf-inner -->
                                                    
                                                </div><!-- /.ld-pf-item -->
                                                
                                            </div><!-- /.col-lg-4 col-md-6 -->
                                    
                                            <div class="lqd-column col-md-4 col-sm-6 col-xs-12 masonry-item">
                                                
                                                <div class="ld-pf-item ld-pf-light pf-details-inside pf-details-full pf-details-h-mid pf-details-v-mid title-size-32 ld-pf-semiround">
                                                    
                                                    <div class="ld-pf-inner">
                                                        
                                                        <div class="ld-pf-image">
                                                            <figure style="background-image: url('{{asset('gambar/jambatan.jpg')}}');">
                                                                <img src="{{asset('gambar/jambatan.jpg')}}" alt="Portfolio Image">
                                                            </figure>
                                                        </div><!-- /.ld-pf-image -->
                                    
                                                        <div class="ld-pf-bg bg-gradient-primary-bl opacity-08"></div><!-- /.ld-pf-bg -->
                                                        
                                                        <div class="ld-pf-details" data-custom-animations="true" data-ca-options='{ "triggerHandler": "mouseenter", "triggerTarget": ".ld-pf-item", "triggerRelation": "closest", "offTriggerHandler": "mouseleave", "animationTarget": ".split-inner", "duration": 200, "delay": 35, "offDuration": 100, "easing": "easeOutCubic", "initValues": { "translateX": 50, "rotateZ": -75, "opacity": 0, "transformOrigin": [0, "-100%", 0] }, "animations": { "translateX": 0, "rotateZ": 0, "opacity": 1, "transformOrigin": [0, "0%", 0] } }'>
                                                            <div class="ld-pf-details-inner">
                                                                <h3 class="ld-pf-title h4 font-weight-semibold" data-split-text="true" data-split-options='{ "type": "chars" }'>Discover</h3>
                                                            </div><!-- /.ld-pf-details-inner -->
                                                        </div><!-- /.ld-pf-details -->
                                                        
                                                        <a href="#" class="liquid-overlay-link"></a>
                                                        
                                                    </div><!-- /.ld-pf-inner -->
                                                    
                                                </div><!-- /.ld-pf-item -->
                                                
                                            </div><!-- /.col-lg-4 col-md-6 -->
                                    
                                            <div class="lqd-column col-md-4 col-sm-6 col-xs-12 masonry-item">
                                                
                                                <div class="ld-pf-item ld-pf-light pf-details-inside pf-details-full pf-details-h-mid pf-details-v-mid title-size-32 ld-pf-semiround">
                                                    
                                                    <div class="ld-pf-inner">
                                                        
                                                        <div class="ld-pf-image">
                                                            <video style="width:100%;height:10%;">
                                                            {{-- <figure style="background-image: url('{{asset('assets/demo/portfolio/pf-13.jpg')}}');"> --}}
                                                                {{-- <img src="{{asset('assets/demo/portfolio/pf-13.jpg')}}" alt="Portfolio Image"> --}}
                                                            <source src="{{asset('gambar/1.MOV')}}" type="video/mp4">
                                                            </video>
                                                            {{-- </figure> --}}
                                                        </div><!-- /.ld-pf-image -->
                                    
                                                        {{-- <div class="ld-pf-bg bg-gradient-primary-bl opacity-08"></div><!-- /.ld-pf-bg --> --}}
                                                        
                                                        {{-- <div class="ld-pf-details" data-custom-animations="true" data-ca-options='{ "triggerHandler": "mouseenter", "triggerTarget": ".ld-pf-item", "triggerRelation": "closest", "offTriggerHandler": "mouseleave", "animationTarget": ".split-inner", "duration": 200, "delay": 35, "offDuration": 100, "easing": "easeOutCubic", "initValues": { "translateX": 50, "rotateZ": -75, "opacity": 0, "transformOrigin": [0, "-100%", 0] }, "animations": { "translateX": 0, "rotateZ": 0, "opacity": 1, "transformOrigin": [0, "0%", 0] } }'>
                                                            <div class="ld-pf-details-inner">
                                                                <h3 class="ld-pf-title h4 font-weight-semibold" data-split-text="true" data-split-options='{ "type": "chars" }'>Discover</h3>
                                                            </div><!-- /.ld-pf-details-inner -->
                                                        </div><!-- /.ld-pf-details --> --}}
                                                        
                                                        <a href="#" class="liquid-overlay-link"></a>
                                                        
                                                    </div><!-- /.ld-pf-inner -->
                                                    
                                                </div><!-- /.ld-pf-item -->
                                                
                                            </div><!-- /.col-lg-4 col-md-6 -->
                                    
                                            <div class="lqd-column col-md-4 col-sm-6 col-xs-12 masonry-item">
                                                
                                                <div class="ld-pf-item ld-pf-light pf-details-inside pf-details-full pf-details-h-mid pf-details-v-mid title-size-32 ld-pf-semiround">
                                                    
                                                    <div class="ld-pf-inner">
                                                        
                                                        <div class="ld-pf-image">
                                                            <figure style="background-image: url('{{asset('assets/img/gambar/12.jpg')}}');">
                                                                <img src="{{asset('assets/img/gambar/12.jpg')}}" alt="Portfolio Image">
                                                            </figure>
                                                        </div><!-- /.ld-pf-image -->
                                    
                                                        <div class="ld-pf-bg bg-gradient-primary-bl opacity-08"></div><!-- /.ld-pf-bg -->
                                                        
                                                        <div class="ld-pf-details" data-custom-animations="true" data-ca-options='{ "triggerHandler": "mouseenter", "triggerTarget": ".ld-pf-item", "triggerRelation": "closest", "offTriggerHandler": "mouseleave", "animationTarget": ".split-inner", "duration": 200, "delay": 35, "offDuration": 100, "easing": "easeOutCubic", "initValues": { "translateX": 50, "rotateZ": -75, "opacity": 0, "transformOrigin": [0, "-100%", 0] }, "animations": { "translateX": 0, "rotateZ": 0, "opacity": 1, "transformOrigin": [0, "0%", 0] } }'>
                                                            <div class="ld-pf-details-inner">
                                                                <h3 class="ld-pf-title h4 font-weight-semibold" data-split-text="true" data-split-options='{ "type": "chars" }'>Discover</h3>
                                                            </div><!-- /.ld-pf-details-inner -->
                                                        </div><!-- /.ld-pf-details -->
                                                        
                                                        <a href="#" class="liquid-overlay-link"></a>
                                                        
                                                    </div><!-- /.ld-pf-inner -->
                                                    
                                                </div><!-- /.ld-pf-item -->
                                                
                                            </div><!-- /.col-lg-4 col-md-6 -->
                                            
                                            <div class="lqd-column col-md-4 col-sm-6 col-xs-12 masonry-item">
                                                
                                                <div class="ld-pf-item ld-pf-light pf-details-inside pf-details-full pf-details-h-mid pf-details-v-mid title-size-32 ld-pf-semiround">
                                                    
                                                    <div class="ld-pf-inner">
                                                        
                                                        <div class="ld-pf-image">
                                                            <figure style="background-image: url('{{asset('assets/img/gambar/22.jpg')}}');">
                                                                <img src="{{asset('assets/img/gambar/22.jpg')}}" alt="Portfolio Image">
                                                            </figure>
                                                        </div><!-- /.ld-pf-image -->
                                    
                                                        <div class="ld-pf-bg bg-gradient-primary-bl opacity-08"></div><!-- /.ld-pf-bg -->
                                                        
                                                        <div class="ld-pf-details" data-custom-animations="true" data-ca-options='{ "triggerHandler": "mouseenter", "triggerTarget": ".ld-pf-item", "triggerRelation": "closest", "offTriggerHandler": "mouseleave", "animationTarget": ".split-inner", "duration": 200, "delay": 35, "offDuration": 100, "easing": "easeOutCubic", "initValues": { "translateX": 50, "rotateZ": -75, "opacity": 0, "transformOrigin": [0, "-100%", 0] }, "animations": { "translateX": 0, "rotateZ": 0, "opacity": 1, "transformOrigin": [0, "0%", 0] } }'>
                                                            <div class="ld-pf-details-inner">
                                                                <h3 class="ld-pf-title h4 font-weight-semibold" data-split-text="true" data-split-options='{ "type": "chars" }'>Discover</h3>
                                                            </div><!-- /.ld-pf-details-inner -->
                                                        </div><!-- /.ld-pf-details -->
                                                        
                                                        <a href="#" class="liquid-overlay-link"></a>
                                                        
                                                    </div><!-- /.ld-pf-inner -->
                                                    
                                                </div><!-- /.ld-pf-item -->
                                                
                                            </div><!-- /.col-lg-4 col-md-6 -->
                                            
                                            <div class="lqd-column col-md-4 col-sm-6 col-xs-12 masonry-item">
                                                
                                                <div class="ld-pf-item ld-pf-light pf-details-inside pf-details-full pf-details-h-mid pf-details-v-mid title-size-32 ld-pf-semiround">
                                                    
                                                    <div class="ld-pf-inner">
                                                        
                                                        <div class="ld-pf-image">
                                                            <figure style="background-image: url('{{asset('gambar/44.jpg')}}');">
                                                                <img src="{{asset('gambar/44.jpg')}}" alt="Portfolio Image">
                                                            </figure>
                                                        </div><!-- /.ld-pf-image -->
                                    
                                                        <div class="ld-pf-bg bg-gradient-primary-bl opacity-08"></div><!-- /.ld-pf-bg -->
                                                        
                                                        <div class="ld-pf-details" data-custom-animations="true" data-ca-options='{ "triggerHandler": "mouseenter", "triggerTarget": ".ld-pf-item", "triggerRelation": "closest", "offTriggerHandler": "mouseleave", "animationTarget": ".split-inner", "duration": 200, "delay": 35, "offDuration": 100, "easing": "easeOutCubic", "initValues": { "translateX": 50, "rotateZ": -75, "opacity": 0, "transformOrigin": [0, "-100%", 0] }, "animations": { "translateX": 0, "rotateZ": 0, "opacity": 1, "transformOrigin": [0, "0%", 0] } }'>
                                                            <div class="ld-pf-details-inner">
                                                                <h3 class="ld-pf-title h4 font-weight-semibold" data-split-text="true" data-split-options='{ "type": "chars" }'>Discover</h3>
                                                            </div><!-- /.ld-pf-details-inner -->
                                                        </div><!-- /.ld-pf-details -->
                                                        
                                                        <a href="#" class="liquid-overlay-link"></a>
                                                        
                                                    </div><!-- /.ld-pf-inner -->
                                                    
                                                </div><!-- /.ld-pf-item -->
                                                
                                            </div><!-- /.col-lg-4 col-md-6 -->
                                            
                                        </div><!-- /.row liquid-portfolio-list-row -->
                                        
                                    </div><!-- /.liquid-portfolio-list -->
        
                            </div><!-- /.col-md-12 -->
        
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>

			</main><!-- /#content.content -->
@include('base.footer')

		</div><!-- /#wrap -->
@endsection
