@extends('base.landing')
<link href="https://fonts.googleapis.com/css2?family=Lora:wght@700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Hammersmith+One&display=swap" rel="stylesheet">

@section('content')
		<div id="wrap">
            @include('base.header')

			<main id="content" class="content">

				<section class="vc_row ">
					<div class="container">
						<div class="row mt-5">
							
							<div class="lqd-column col-md-12 text-center">

								<div class="lqd-column-inner border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">

									<div class="row d-flex flex-wrap align-items-center">
	
										<div class="lqd-column col-md-12 text-center">
                                            <h2 class="text-center" style="font-family: 'Hammersmith One', sans-serif;font-style: italic; " >" Come and Experience the Treasure of Terengganu"</h2>
                                            
										</div><!-- /.lqd-column col-md-4 -->
	
										{{-- <div class="lqd-column col-md-8 text-md-right">
											<ul class="lqd-custom-menu reset-ul inline-nav mb-md-0">
												<li><a href="#">Kuala Terengganu Draw Bridge</a></li>
												<li><a href="#">Taman Tamadun Islam</a></li>
												<li><a href="#">Duyung Marina Bay</a></li>
												<li><a href="#">Pulau Warisan</a></li>
											</ul>
										</div><!-- /.lqd-column col-md-8 --> --}}
	
									</div><!-- /.row -->

								</div><!-- /.lqd-column-inner -->

							</div><!-- /.lqd-column col-md-12 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
                </section>
                
                {{-- //single rider --}}
<form method="GET" action="{{route('single')}}">
                <section class="vc_row pt-50 pb-50" >
					<div class="container border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">
                            <div class="row d-flex flex-wrap">
                                <div class="lqd-column col-sm-6 col-md-offset-5 text-center"><h4 class="font-weight-bold">SINGLE RIDER</h4>
                                    <p>Experience : Alone Ride</p>
                                </div>
                            </div>
						<div class="row d-flex flex-wrap align-items-center mb-3">

							<div class="lqd-column col-md-5  ">
                                <div class="liquid-img-group-single mb-3" data-reveal="true" data-reveal-options='{"direction":"lr","bgcolor":"","delay":""}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid">
										</div><!-- /.liquid-img-group-content --> 
										<div class="liquid-img-container-inner">
											<figure>
											{{-- <img width="1141" height="760" class="single-img" src="{{asset('assets/img/gambar/12.jpg')}}" alt="On your own, you see. On a tour, you do." />  --}}
											<img width="1141" height="760" class="single-img" src="{{asset('assets/img/galery/11.jpeg')}}" alt="On your own, you see. On a tour, you do." /> 
											</figure> 
										</div><!-- /.liquid-img-container-inner -->
								 </div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single --> 


							</div><!-- /.lqd-column col-md-5 -->

							<div class="lqd-column col-md-5 col-md-offset-1">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Select date</label>
                                        <input type="date" name="date" class="form-controls">
                                    </div>
                                    <div class="form-group">
                                    <label>
                                        Select Type
                                    </label>
                                    <select class="form-control single-rider" name="jetski">
                                        <option value="0">-Choose Jetski-</option>
                                        @foreach ($package as $item)
                                        @if ($item->rider == 1)   
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    </div>
                                </div>
                                {{-- <div class="col-md-6">
                                    <div class="description">
                                    </div>
                                </div> --}}
                                <div class="col-md-6">
                                    <div class="form-check px-4">
                                        <label class="form-check-label" for="flexCheckDefault">
                                        <input class="form-check-input drone-single" type="checkbox" name="drone" id="flexCheckDefault">
                                          Add Drone Shot
                                        </label>
                                      </div>
                                    <div class="description-drone">
                                        <ul>
                                            <li>Up to 20 minutes</li>
                                            <li>Picture and Video</li>
                                            <li>Capture While Riding</li>
                                            <li>RM 200</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 offset-md-6">
                                    <div class="description">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <input type="hidden" value="" class="singleprice">
                                <div class="col-md-6 col-md-offset-6 total-single">
                                    Total : RM 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6 total">
                                   <button type="submit" class="btn btn-primary py-2 px-5 mx-2">Book</button>
                                </div>
                            </div>

							

							</div><!-- /.lqd-column col-md-6 col-md-offset-1 -->

                        </div><!-- /.row -->
                      
					</div><!-- /.container -->
				</section> 
</form>
<form method="GET" action="{{route('double')}}">
                <section class="vc_row pt-50 pb-50" >
					<div class="container border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">
                            <div class="row d-flex flex-wrap">
                                <div class="lqd-column col-sm-6 col-md-offset-5 text-center"><h4 class="font-weight-bold">DOUBLE RIDER</h4>
                                    <p>Enjoy Couple Ride</p>
                                </div>
                            </div>
						<div class="row d-flex flex-wrap align-items-center mb-3">

							<div class="lqd-column col-md-5  ">
                                <div class="liquid-img-group-single mb-3" data-reveal="true" data-reveal-options='{"direction":"lr","bgcolor":"","delay":""}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid">
										</div><!-- /.liquid-img-group-content --> 
										<div class="liquid-img-container-inner">
											<figure>
											{{-- <img width="1141" height="760" class="double-img" src="{{asset('assets/img/gambar/about.jpg')}}" alt="On your own, you see. On a tour, you do." />  --}}
											<img width="1141" height="760" class="double-img" src="{{asset('assets/img/galery/2.jpeg')}}" alt="On your own, you see. On a tour, you do." /> 
											</figure> 
										</div><!-- /.liquid-img-container-inner -->
								 </div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single --> 


							</div><!-- /.lqd-column col-md-5 -->

							<div class="lqd-column col-md-5 col-md-offset-1">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Select date</label>
                                    <input type="date" name="date" class="form-controls">
                                    </div>
                                    <div class="form-group">
                                    <label>
                                        Select Type
                                    </label>
                                    <select class="form-control double-rider" name="jetski">
                                        <option value="0">-Choose Jetski-</option>
                                        @foreach ($package as $item)
                                        @if ($item->rider == 2)   
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-check px-4">
                                        <label class="form-check-label" >
                                        <input class="form-check-input drone-double" type="checkbox" value="" >
                                          Add Drone Shot
                                        </label>
                                      </div>
                                        <div class="description-for-specialtrip">
                                            <ul>
                                                <li>Up to 20 minutes</li>
                                                <li>Picture and Video</li>
                                                <li>Capture While Riding</li>
                                                <li>RM 200</li>
                                            </ul>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="description-double">
                                    </div> 
                                </div>
                            </div>
                            <div class="row">
                                <input type="hidden" value="" class="doubleprice">
                                <div class="col-md-6 col-md-offset-6 total-double">
                                    Total : RM 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6 total">
                                   <button type="submit" class="btn btn-primary py-2 px-5 mx-2">Book</button>
                                </div>
                            </div>
							

							</div><!-- /.lqd-column col-md-6 col-md-offset-1 -->

                        </div><!-- /.row -->
                      
					</div><!-- /.container -->
                </section> 
</form>
                <section class="vc_row pt-50 pb-50" >
					<div class="container border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">
                            <div class="row d-flex flex-wrap">
                                <div class="lqd-column col-sm-12 text-center"><h4 class="font-weight-bold text-uppercase">Special Trip</h4>
                                    <p class="text-capitalize">Ride alone to explore the magnificient of kapas island</p>
                                </div>
                            </div>
                            <form method="GET" action="{{route('kapas')}}">
						<div class="row d-flex flex-wrap align-items-center mb-3">

							<div class="lqd-column col-md-5  ">
                                <div class="liquid-img-group-single mb-3" data-reveal="true" data-reveal-options='{"direction":"lr","bgcolor":"","delay":""}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid">
										</div><!-- /.liquid-img-group-content --> 
										<div class="liquid-img-container-inner">
											<figure>
											<img width="1141" height="760" src="{{asset('assets/img/kapas/kapas.jpg')}}" alt="On your own, you see. On a tour, you do." /> 
											</figure> 
										</div><!-- /.liquid-img-container-inner -->
								 </div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single --> 


							</div><!-- /.lqd-column col-md-5 -->

                           
							<div class="lqd-column col-md-5 col-md-offset-1">
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Select date</label><br>
                                        <input type="date" name="date" class="form-controls">
                                    </div>
                                    <label>
                                        Select Type
                                    </label>
                                    <select class="form-control kapas-jetski" name="jetski" >
                                        <option value="0">-Choose Jetski-</option>
                                        @foreach ($package as $item)
                                        @if ($item->rider == 3)   
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Description :</label>
                                    <ul>
                                        <li>8 hours 8am - 4pm </li>
                                        <li>Life jacket Provided</li>
                                        <li>Thursday to sunday only available for booking</li>
                                    </ul>

                                </div>
                            </div>
                            <div class="row">
                                <input type="hidden" value="" class="kapasprice">
                                <div class="col-md-6 col-md-offset-6 total-kapas">
                                    Total : RM 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6 ">
                                   <button type="submit" class="btn btn-primary py-2 px-5 mx-2">Book</button>
                                </div>
                            </div>

                        </div><!-- /.lqd-column col-md-6 col-md-offset-1 -->
                        
                    </div><!-- /.row -->
                </form>
                      
					</div><!-- /.container -->
				</section> 



			</main><!-- /#content.content -->
@include('base.footer')

		</div><!-- /#wrap -->
@endsection
@section('script')
<script>
    //single rider
$('.single-rider').change(function(e){
    var id =$(this).val();
    $.get(`{{url('publicpackage/${id}')}}`)
    .done(function(data){
        // console.log(data);
        //set single price
        $('.singleprice').val(data.price)
        //set image
        // $('.single-img').attr("src", `{{url('${data.image}')}}`);
        $('.description').empty();
        $('.description').append(`<ul> ${data.description}</ul>`);
        if($('.drone-single').is(':checked'))
        {
            $('.total-single').html(`Total :RM ${parseInt(data.price)+200}`)
        }else{
            $('.total-single').html(`Total :RM ${data.price}`)
        }
        
    })
})
$('.drone-single').change(function(e){
    var price =$('.singleprice').val();
    if($('.drone-single').is(':checked'))
    {
            if(price.length > 0){
            $('.total-single').html(`Total :RM ${parseInt(price)+200}`)
            }else{
            $('.total-single').html(`Total :RM ${200}`)
            }
        }else{
            $('.total-single').html(`Total :RM ${price}`)
        }
})
        
  //double rider
  $('.double-rider').change(function(e){
    var id =$(this).val();
    $.get(`{{url('publicpackage/${id}')}}`)
    .done(function(data){
        // console.log(data);
        //set double price
        $('.doubleprice').val(data.price)
        $('.description-double').empty();
        $('.description-double').append(`<ul> ${data.description}</ul>`);
        if($('.drone-double').is(':checked'))
        {
            $('.total-double').html(`Total :RM ${parseInt(data.price)+200}`)
        }else{
            $('.total-double').html(`Total :RM ${data.price}`)
        }
        
    })
})
$('.drone-double').change(function(e){
    var price =$('.doubleprice').val();
    if($('.drone-double').is(':checked'))
    {
            if(price.length > 0){
            $('.total-double').html(`Total :RM ${parseInt(price)+200}`)
            }else{
            $('.total-double').html(`Total :RM ${200}`)
            }
        }else{
            $('.total-double').html(`Total :RM ${price}`)
        }
})


$('.kapas-jetski').change(function(e){
    var id =$(this).val();
    $.get(`{{url('publicpackage/${id}')}}`)
    .done(function(data){
        // console.log(data);
        //set single price
        $('.kapasprice').val(data.price)
        //set image
        // $('.single-img').attr("src", `{{url('${data.image}')}}`);
        // $('.description').empty();
        // $('.description').append(`<ul> ${data.description}</ul>`);
        // if($('.drone-single').is(':checked'))
        // {
            $('.total-kapas').html(`Total :RM ${parseInt(data.price)}`)
        // }else{
        //     $('.total-single').html(`Total :RM ${data.price}`)
        // }
        
    })
})
</script>
@endsection
