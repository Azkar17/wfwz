@extends('base.landing')
@section('content')

<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@900&display=swap" rel="stylesheet">

<link href="https://fonts.googleapis.com/css2?family=Hammersmith+One&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@1,300&family=Hammersmith+One&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Lora:wght@700&display=swap" rel="stylesheet">
<style>
	.header-font{
		font-family: 'Roboto', sans-serif;
	}
	.jet{
		color: white; font-size:70px;
	}
}
</style>
	<div id="wrap">
            @include('base.header')
			<main id="content" class="content">
				{{-- <section class="vc_row  fullheight d-flex align-items-center py-8" data-parallax="true"
					data-parallax-options='{"parallaxBG": true}' style="background-color: black;">

					<span class="row-bg-loader"></span>

					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">

							<div
							class="lqd-column col-md-8 col-md-offset-2 text-center mt-md-9"
							data-custom-animations="true"
							data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1400,"delay":"200","startDelay": 250,"easing":"easeOutQuint","direction":"forward","initValues":{"translateY":45,"opacity":0},"animations":{"translateY":0,"opacity":1}}'>

								<div class="ld-fancy-heading text-uppercase ltr-sp-2">
									<h6
									class="text-white"
									data-split-text="true"
									data-split-options='{"type":"words"}'>
										<span class="ld-fh-txt header-font" style="font-size: 30px;"> 1# Watersport In Terengganu </span>
									</h6>
								</div><!-- /.ld-fancy-heading -->

								<figure class="mb-30">
									<h1  class="header-font jet">JETSKI TOUR ADVENTURE</h1>
								</figure>
							
								<div class="ld-fancy-heading mb-4">
									<p class="font-size-18 text-white">
									
									</p>
								</div><!-- /.ld-fancy-heading -->

                            <a href="{{url('/packagenew')}}" class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-2">
									<span>
										<span class="btn-txt">EXPLORE</span>
									</span>
								</a>

							</div><!-- /.lqd-column col-md-8 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
				</section> --}}
				<section class="vc_row mt-9">
					<video autoplay muted loop id="myVideo" style="position:static;
						top: 0;
						l: 0;
						min-width: 100%;
						min-height: 100%;
						z-index:0;">
							<source src="{{asset('assets/img/gambar/hompagevideo.mp4')}}" type="video/mp4">
							</video>
						<div class="col-md-12 mt-2" style="z-index: 1;position:absolute;top:0">
							<div
							class="lqd-column col-md-8 col-md-offset-2 text-center mt-md-9"
							data-custom-animations="true"
							data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1400,"delay":"200","startDelay": 250,"easing":"easeOutQuint","direction":"forward","initValues":{"translateY":45,"opacity":0},"animations":{"translateY":0,"opacity":1}}'>

								<div class="ld-fancy-heading text-uppercase ltr-sp-2">
									<h6
									class="text-white"
									data-split-text="true"
									data-split-options='{"type":"words"}'>
										<span class="ld-fh-txt header-font" style="font-size: 30px; color:black;" >  Watersport In Terengganu </span>
									</h6>
								</div><!-- /.ld-fancy-heading -->

								<figure class="mb-2">
									<h1  class="header-font jet" style="font-family: 'Lora',serif;color:rgb(0, 0, 0);">JETSKI TOUR ADVENTURE</h1>
								</figure>
							
                            <a href="{{url('#gallery')}}" class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-2 mb-2">
									<span class="btn-txt">EXPLORE</span>
								</a>

							</div><!-- /.lqd-column col-md-8 -->
						</div>
						</div><!-- /.row -->
					
				</section>
		
				

				<section class="vc_row pt-50 pb-50">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">
							<div class="lqd-column col-md-12 mb-30">
								<h2 class="text-center text-uppercase" style="font-family: 'Hammersmith One', sans-serif" >JETSKI ROUTE<br>
									(Explore Terengganu Iconic Places)
								</h2>
								<div class="row">
									<div class="col-md-12">
									<img src="{{asset('assets/img/gambar/MAP-JETSKI2.jpg')}}" alt="map">
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>








				{{-- activities --}}
				
				<section class="vc_row pt-50 pb-50" id="about">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">
							<div class="lqd-column col-md-12 mb-30">
								<h2 class="text-center" style="font-family: 'Hammersmith One', sans-serif" >ACTIVITIES</h2>
								<div class="row">
									<div class="col-sm-6">
										{{-- <h4 style="z-index:2;">Best Seller !!!</h4> --}}
										<div class="fancy-box content-box-heading-sm fancy-box-tour">
											<figure class="fancy-box-image loaded">
											<img width="564" height="500" alt="" style="height:300px;" src="{{asset('assets/activities/s.jpeg')}}">
											</figure>
											<div class="fancy-box-contents">
												<div class="fancy-box-header text-center">
													<a href=""><h3 class="font-weight-semibold text-uppercase">Single rider</h3></a>
												</div><!-- /.fancy-box-header -->
												<div class="fancy-box-info text-center">
												<a href="{{url('/packagenew')}}" class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-3 py-2">Book A Ride</a>
												</div><!-- /.fancy-box-info -->
											</div><!-- /.fancy-box-contents -->
										</div><!-- /.fancy-box -->
									</div>
									<div class="col-sm-6">
										<div class="fancy-box content-box-heading-sm fancy-box-tour">
											
											<figure class="fancy-box-image loaded">
											<img width="564" height="500" alt="" style="height:300px;" src="{{asset('assets/img/gambar/double.jpeg')}}">
											</figure>
											<div class="fancy-box-contents">
												<div class="fancy-box-header text-center">
													<a href=""><h3 class="font-weight-semibold text-uppercase">Double rider</h3></a>
												</div><!-- /.fancy-box-header -->
												<div class="fancy-box-info text-center">
													<a href="{{url('/packagenew')}}" class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-3 py-2">Book A Ride</a>
												</div><!-- /.fancy-box-info -->
											</div><!-- /.fancy-box-contents -->
										</div><!-- /.fancy-box -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>






				<section class="vc_row bg-cover fullheight d-flex align-items-center"  style="background-image: url({{asset('assets/img/gambar/kapas.jpeg')}});  background-repeat: no-repeat; background-size: cover;">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">
							<div class="lqd-column col-md-12 mb-30">
								<h1 class="text-center text-white text-bold text-uppercase" style="margin-bottom:-20px; font-family: 'Hammersmith One', sans-serif; font-size:100px;" >Trip To Pulau Kapas !!!</h1>
								<h6 class="text-center text-white">(EYE CATCHING OF KAPAS ISLAND) </h6>
								<div class="row">
									<div class="col-sm-12">
										<div class="text-center">
											<h5 class="text-white text-italic" style="font-family: 'Barlow', sans-serif;">LOVE TO EXPLORE CORAL REEF UNDERWATER ?? <br>
											YOU ALSO CAN CATCH OUT THE WONDERFUL MARINE LIFE</h5>
											<div class="text-center">
												<a href="{{url('/packagenew')}}" class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-4 py-2">Book A Ride</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</section>
				
				{{-- how to find us --}}
				<section class="vc_row pt-90 pb-90 mb-50 mt-50 ">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">

							<div class="lqd-column col-md-12">

								<header class="lqd-fancy-title mb-md-0 text-center">
									<h2 class="my-0 text-uppercase " style="font-family: 'Hammersmith One', sans-serif;">How To Find Us ?</h2>
								</header><!-- /.lqd-fancy-title -->
								
							</div>
						</div>
							<div class="row">
								<div class="lqd-column col-md-12 p-5">
									<header class="lqd-fancy-title mb-md-0 text-center">
								<a href="{{url('/packagenew')}}" class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-4 py-2">
									Book a ride today
								</a>
									</header>
								</div>
							</div>
							<div class="row">
								<div class="lqd-column col-md-12 p-5">
									<header class="lqd-fancy-title mb-md-0 text-center">
										<div>
											<h4 class="text-uppercase">WISMA WFWZ</h4>
											<p>Tingkat 1,Lot 3215 Jalan Bukit Kechil,<br>
												Kg Bukit Kechil, 21100 Kuala Terengganu<br>
												MALAYSIA
											</p>
										</div>
									<ul style="list-style:none;">
										<li>Email : enquiry@wfwzinsight.com.my   </li>
										<li>Phone : 0163419366</li>
										<li>Facebook : <a href="https://www.facebook.com/sewajetski">https://www.facebook.com/sewajetski</a></li>
										<li>Instagram : <a href="https://www.instagram.com/wfwzinsight/">https://www.instagram.com/wfwzinsight/</a></li>
										<li>Whatsapp : <a href="https://watsap.my/60163419366">https://watsap.my/60163419366</a></li>
									</ul>
										
									</header>
								</div>
							</div>
							<div class="row">
								<div class="lqd-column col-md-4">
									<div class="card">
										{{-- <img src="{{asset('assets/img/gambar/p1.jpeg')}}" > --}}
									</div>
								</div>
								<div class="lqd-column col-md-4">
									<div class="card">
										<img src="{{asset('assets/img/gambar/p2.jpeg')}}" >
									</div>
								</div>
								<div class="lqd-column col-md-4">
									<div class="card">
										{{-- <img src="{{asset('assets/img/gambar/p3.jpeg')}}" > --}}
									</div>
								</div>
								</div>
							</div>
						</div>
					
				</section>
				{{-- gallery --}}
				<section class="vc_row pt-90 pb-90 mb-50 mt-50" id="gallery">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">

							<div class="lqd-column col-md-12">

								<header class="lqd-fancy-title mb-md-0 text-center">
									<h2 class="my-0 text-uppercase ">Our Gallery</h2>
								</header><!-- /.lqd-fancy-title -->

							</div><!-- /.lqd-column col-md-8 -->
								<div class="lqd-column col-md-12 mt-30">
								<div class="carousel-container carousel-nav-floated carousel-nav-center carousel-nav-middle carousel-nav-md carousel-nav-solid carousel-nav-rectangle carousel-nav-shadowed carousel-dots-style1">
									<div class="carousel-items row mx-0" data-lqd-flickity='{"cellAlign":"center","prevNextButtons":true,"buttonsAppendTo":"self","pageDots":false,"groupCells":true,"wrapAround":true,"pauseAutoPlayOnHover":false,"navArrow":"1","navOffsets":{"nav":{"top":"25%"},"prev":"-13px","next":"-13px"}}'>
										{{-- @forelse ($package as $item) --}}
                                        {{-- <div class="carousel-item col-sm-8 col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('gambar/1.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 --> --}}
                                        <div class="carousel-item col-sm-8 col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('gambar/2.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm-8 col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('gambar/3.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                         <div class="carousel-item col-sm-8 col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('gambar/4.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 --> 
                                        <div class="carousel-item col-sm-8 col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/13.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/gambar/12.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/1.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/2.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/3.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        {{-- <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/4.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 --> --}}
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/5.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/6.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/7.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/8.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/9.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/10.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/12.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/13.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/galery/14.jpeg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->




									</div><!-- /.carousel-items -->
								</div><!-- /.carousel-container -->

							</div><!-- /.lqd-column col-md-12 -->

						</div><!-- /.row d-flex flex-wrap align-items-center -->
					</div><!-- /.container -->
				</section>
			
			</main><!-- /#content.content -->

            @include('base.footer')

        </div><!-- /#wrap -->
        
		@endsection
		@section('script')
		@if($errors->has('clash'))
		<script>
				Swal.fire({
				position: 'top-end',
				icon: 'error',
				title: 'Sorry Somebody Already Book Selected Time 😥 Change To Other Time',
				showConfirmButton: false,
				timer: 2500
				})
		</script>    
		@endif

		@if(session('success'))
		 <script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: `{{session('success')}}`,
			showConfirmButton: false,
			timer: 2500
			})
		</script>    
		 @endif
		@if(session('cancel'))
		 <script>
			Swal.fire({
			position: 'top-end',
			icon: 'success',
			title: `{{session('cancel')}}`,
			showConfirmButton: false,
			timer: 2500
			})
		</script>    
		 @endif


		@endsection
