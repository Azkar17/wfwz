@extends('base.landing')
@section('content')
		<div id="wrap">
            @include('base.header')

			<main id="content" class="content">

				<section class="vc_row ">
					<div class="container">
						<div class="row mt-5">
							
							<div class="lqd-column col-md-12 text-center">

								<div class="lqd-column-inner border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">

									<div class="row d-flex flex-wrap align-items-center">
	
										<div class="lqd-column col-md-12 text-center">
                                            <h2 class="text-center" >"Special Trip !!"</h2>
                                            <p class="text-capltalize">Ride alone to explore the magnificient of kapas island</p>
                                            
										</div><!-- /.lqd-column col-md-4 -->
	
	
									</div><!-- /.row -->

								</div><!-- /.lqd-column-inner -->

							</div><!-- /.lqd-column col-md-12 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
                </section>
                
                {{-- //single rider --}}

                <section class="vc_row pt-50 pb-50" >
					<div class="container border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">
                            <div class="row d-flex flex-wrap">
                                <div class="lqd-column col-sm-6 col-md-offset-5 text-center"><h4 class="font-weight-bold">SINGLE RIDER</h4>
                                    <p>Experience : Alone Ride</p>
                                </div>
                            </div>
						<div class="row d-flex flex-wrap align-items-center mb-3">

							<div class="lqd-column col-md-5  ">
                                <div class="liquid-img-group-single mb-3" data-reveal="true" data-reveal-options='{"direction":"lr","bgcolor":"","delay":""}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid">
										</div><!-- /.liquid-img-group-content --> 
										<div class="liquid-img-container-inner">
											<figure>
											<img width="1141" height="760" src="{{asset('assets/img/gambar/about.jpg')}}" alt="On your own, you see. On a tour, you do." /> 
											</figure> 
										</div><!-- /.liquid-img-container-inner -->
								 </div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single --> 


							</div><!-- /.lqd-column col-md-5 -->

							<div class="lqd-column col-md-5 col-md-offset-1">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Select date</label>
                                    <input type="date" name="date" class="form-controls">
                                    <label>
                                        Select Type
                                    </label>
                                    <select class="form-control" name="jetski">
                                        <option>900cc</option>
                                        <option>260HP</option>
                                        <option>260HP</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <div class="description">
                                        <ul>
                                            <li>light weight jetski Rm 100</li>
                                            <li>Super Charged</li>
                                            <li>Performance Jetski Rm 180</li>
                                            <li>Super Charged</li>
                                            <li>Performance Jetski Rm 200</li>
                                            <li>Stage 2 Mode</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Add drone Shot</label>
                                    <input type="checkbox" name="drone" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <div class="description">
                                        <ul>
                                            <li>Up to 20 minutes</li>
                                            <li>Picture and Video</li>
                                            <li>Capture While Riding</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6 total">
                                    Total : RM 
                                </div>
                            </div>

							

							</div><!-- /.lqd-column col-md-6 col-md-offset-1 -->

                        </div><!-- /.row -->
                      
					</div><!-- /.container -->
				</section> 

                <section class="vc_row pt-50 pb-50" >
					<div class="container border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">
                            <div class="row d-flex flex-wrap">
                                <div class="lqd-column col-sm-6 col-md-offset-5 text-center"><h4 class="font-weight-bold">Double RIDER</h4>
                                    <p>Enjoy Couple Ride</p>
                                </div>
                            </div>
						<div class="row d-flex flex-wrap align-items-center mb-3">

							<div class="lqd-column col-md-5  ">
                                <div class="liquid-img-group-single mb-3" data-reveal="true" data-reveal-options='{"direction":"lr","bgcolor":"","delay":""}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid">
										</div><!-- /.liquid-img-group-content --> 
										<div class="liquid-img-container-inner">
											<figure>
											<img width="1141" height="760" src="{{asset('assets/img/gambar/about.jpg')}}" alt="On your own, you see. On a tour, you do." /> 
											</figure> 
										</div><!-- /.liquid-img-container-inner -->
								 </div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single --> 


							</div><!-- /.lqd-column col-md-5 -->

							<div class="lqd-column col-md-5 col-md-offset-1">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Select date</label>
                                    <input type="date" name="date" class="form-controls">
                                    <label>
                                        Select Type
                                    </label>
                                    <select class="form-control" name="jetski">
                                        <option>900cc</option>
                                        <option>260HP</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <div class="description">
                                        <ul>
                                            <li>light weight jetski Rm 170</li>
                                            <li>Super Charged</li>
                                            <li>Performance Jetski Rm 330</li>
                                            <li>Discounted 50% off</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Add drone Shot</label>
                                    <input type="checkbox" name="drone" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <div class="description">
                                        <ul>
                                            <li>Up to 20 minutes</li>
                                            <li>Picture and Video</li>
                                            <li>Capture While Riding</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6 total">
                                    Total : RM 
                                </div>
                            </div>

							

							</div><!-- /.lqd-column col-md-6 col-md-offset-1 -->

                        </div><!-- /.row -->
                      
					</div><!-- /.container -->
				</section> 



			</main><!-- /#content.content -->
@include('base.footer')

		</div><!-- /#wrap -->
@endsection
