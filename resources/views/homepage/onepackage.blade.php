@extends('base.landing')
@section('content')
		<div id="wrap">
            @include('base.header')

			<main id="content" class="content">
			
				<section class="vc_row pt-50 pb-10" >
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">

							<div class="lqd-column col-md-5 mb-30">

                            <h2 class="mt-0 mb-3" >{{$package->name}}</h2>
								<!-- <h3 class="mt-0 mb-40 font-size-14 text-uppercase ltr-sp-05 text-secondary">Package Name</h3> -->
								<div class="lqd-h-sep w-10 mb-40">
									<span class="lqd-h-sep-inner"></span><!-- /.lqd-h-sep-inner -->
								</div><!-- /.lqd-h-sep -->

								<p> Description</p>
								<br>
                            <p>{{ $package->description}}<br> RM {{$package->price}} For {{$package->duration}}</p>
								<br>

							</div><!-- /.lqd-column col-md-5 -->

							<div class="lqd-column col-md-6 col-md-offset-1 text-center mb-30 mt-7">

								<div class="liquid-img-group-single mb-3 " data-reveal="true" data-reveal-options='{"direction":"lr","bgcolor":"","delay":""}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid">
										</div><!-- /.liquid-img-group-content -->
										<div class="liquid-img-container-inner">
											<figure>
                                            <img width="1141" height="760" src="{{$package->image}}" style=" width: 100%;height:70%;object-fit:cover" />
											</figure>
										</div><!-- /.liquid-img-container-inner -->
									</div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single -->

								{{-- <p>Ride On Your Own Or We Bring You For The Ride</p> --}}

							</div><!-- /.lqd-column col-md-6 col-md-offset-1 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
				</section>
				{{-- <div class="text-center" style="background-color: red;">
					<h2>Eroor here</h2>
				</div> --}}
				@if(session('success'))
				<div class="alert alert-success text-center" style="background-color: #98FB98" >
					👍 &nbsp;{!! session('success') !!} &nbsp;
				</div>
				@endif
				<h2 class="text-center">Book This Package Now</h2>
                <section class="vc_row " >
					<div class="container">
						<form action="{{route('public.book')}}" method="POST">
							@csrf
						<input type="hidden" name="package" value="{{$package->id}}">
						<div class="row d-flex flex-wrap align-items-center">
                            <div class="lqd-column col-md-12 mb-30">
                                <label> Name</label>
								<input type="text" class="form-control " @error('name')style="border-color: red"@enderror name="name">
								@error('name')<span>{{$message}}</span> @enderror
                            </div>
                        </div>
						<div class="row d-flex flex-wrap align-items-center">
                            <div class="lqd-column col-md-12 mb-30">
                                <label> Email</label>
								<input type="email" class="form-control " @error('email') style="border-color: red"@enderror name="email">
								@error('email')<span>{{$message}}</span> @enderror
                            </div>
                        </div>
						<div class="row d-flex flex-wrap align-items-center">
                            <div class="lqd-column col-md-12 mb-30">
                                <label> Contact</label>
								<input type="text" class="form-control " 	@error('phone')style="border-color: red"@enderror name="phone">
								@error('phone')<span>{{$message}}</span> @enderror
                            </div>
                        </div>
						<div class="row d-flex flex-wrap align-items-center">
                            <div class="lqd-column col-md-6 mb-30">
                                <label> Date</label>
							<input type="date" class="form-control " 	@error('date')style="border-color: red"@enderror name="date" value="{{old('')}}">
								@error('date')<span>{{$message}}</span> @enderror
                            </div>
                            <div class="lqd-column col-md-6 mb-30">
                                <label> Time</label>
								<input type="time" class="form-control " @error('time')style="border-color: red"@enderror name="time">
								@error('time')<span>{{$message}}</span> @enderror
                            </div>
						</div>
						<div class="row d-flex flex-wrap align-items-center justify-content-center">
							<button class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-2 py-2" type="submit">
								Book This Package
							</button>
						</div>
						</form>
                    </div>
                </section>
			</main><!-- /#content.content -->
@include('base.footer')

		</div><!-- /#wrap -->
@endsection
