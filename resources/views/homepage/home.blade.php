@extends('base.home-layout')

@section('content')
     <!-- Navbar -->
  <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
    <div class="container">
      <a class="navbar-brand mr-lg-5" href="#">
      </a>
  
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="wrapper">
    <div class="section section-hero section-shaped">
       <div class="shape shape-style-3 shape-default">
        <span class="span-150"></span>
        <span class="span-50"></span>
        <span class="span-50"></span>
        <span class="span-75"></span>
        <span class="span-100"></span>
        <span class="span-75"></span>
        <span class="span-50"></span>
        <span class="span-100"></span>
        <span class="span-50"></span>
        <span class="span-100"></span>
      </div> 
      <div class="page-header" >
        <div class="container shape-container d-flex align-items-center py-lg">
          <div class="col px-0">
            <div class="row align-items-center justify-content-center">
              <div class="col-lg-10 text-center">
                <h1 class="text-white display-1 ">WFWZ Watersports</h1>
                <h2 class="display-4 font-weight-normal text-white">Don't Miss Enjoy Watersports In Kuala Terengganu Book Your Date  </h2>
                <div class="btn-wrapper mt-4">
                  <button class="btn btn-info btn-icon mt-3 mb-sm-0" data-toggle="modal" data-target="#booking">
                    <span class="btn-inner--icon"><i class="far fa-calendar-alt"></i></span>
                    <span class="btn-inner--text">Book Now</span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    @if(session('success'))
    <div class="alert alert-success text-center" >
        👍 &nbsp;{!! session('success') !!} &nbsp;💖
    </div>
    @endif
   

    <div class="section features-6">
      <div class="container">
        <h2 class="text-center my-2 font-weight-bold text-undeline text-uppercase">Our Package</h2>
        <div class="row justify-content-space-between">
          @forelse ($package as $item)
          <div class="col-lg-4 py-2">
            <div class="card shadow p-3 mb-5 bg-white rounded h-100">
              @if ($item->image == NULL)
              <img class="card-img-top" src="{{{asset('argon/img/pages/1.jpeg')}}}" alt="Card image cap" >
              @else
              <img class="card-img-top" src="{{$item->image}}" alt="Card image cap">
              @endif
                <div class="card-body text-center">
                  <h5 class="card-title font-weight-bold text-uppercase">{{$item->name}}</h5>
                  <p class="card-text ">{{$item->description}}</p>
                  <p class="card-text ">{{$item->duration}} Minutes <br> RM {{$item->price}}</p>
                <button class="btn btn-primary book mt-2" data-toggle="modal" data-target="#booking" data-id="{{$item->id}}">Book Now</button>
                </div>
              </div>
          </div>
          @empty     
          @endforelse
        </div>
      </div>
    </div>

    <div class="section features-6">
      <div class="container">
        <h2 class="text-center my-2 font-weight-bold text-underline text-uppercase">Gallery</h2>
        <div class="row justify-content-space-between ">
          <div class="col-lg-4 py-2">
              <div class="card shadow p-1 mb-5 bg-white rounded text-white">
                <img class="card-img" src="{{{asset('gambar/1.jpg')}}}" alt="Card" >
             </div>
          </div>
          <div class="col-lg-4 py-2">
              <div class="card shadow p-1 mb-5 bg-white rounded text-white">
                <img class="card-img" src="{{{asset('gambar/2.jpg')}}}" alt="Card" >
             </div>
          </div>
          <div class="col-lg-4 py-2">
              <div class="card shadow p-1 mb-5 bg-white rounded text-white">
                <img class="card-img" src="{{{asset('gambar/3.jpg')}}}" alt="Card" >
             </div>
          </div>
          <div class="col-lg-4 py-2">
              <div class="card shadow p-1 mb-5 bg-white rounded text-white">
                <img class="card-img" src="{{{asset('gambar/4.jpg')}}}" alt="Card" >
             </div>
          </div>
          <div class="col-lg-4 py-2">
              <div class="card shadow p-1 mb-5 bg-white rounded text-white">
                <img class="card-img" src="{{{asset('gambar/jambatan.jpg')}}}" alt="Card" >
             </div>
          </div>
          <div class="col-lg-4 py-2">
              <div class="card shadow p-1 mb-5 bg-white rounded text-white">
                <img class="card-img" src="{{{asset('argon/img/pages/1.jpeg')}}}" alt="Card" >
             </div>
          </div>
        </div>
      </div>
    </div>

   
   
    <br /><br />
    <footer class="footer">
      <div class="container">
        <div class="row row-grid align-items-center mb-5">
          <div class="col-lg-6">
            <h3 class="text-primary font-weight-light mb-2">Thank you for supporting us!</h3>
            <h4 class="mb-0 font-weight-bold">Time 3.00 PM Until 7.00 PM</h4>
          </div>
          <div class="col-lg-6 text-lg-center btn-wrapper">
            <a target="_blank" href="https://wa.me/60163419366" rel="nofollow" class="btn btn-icon-only btn-success rounded-circle" data-toggle="tooltip" data-original-title="chat us">
              <span class="btn-inner--icon"><i class="fa fa-whatsapp"></i></span>
            </a>
            <button target="_blank" href="" rel="nofollow" class="btn-icon-only rounded-circle btn btn-facebook" data-toggle="tooltip" data-original-title="Like us">
              <span class="btn-inner--icon"><i class="fab fa-facebook"></i></span>
            </button>
            <button target="_blank" href="" rel="nofollow" class="btn btn-icon-only btn-instagram rounded-circle" data-toggle="tooltip" data-original-title="Follow us">
              <span class="btn-inner--icon"><i class="fa fa-instagram"></i></span>
            </button>
          
          </div>
        </div>
        <hr>
        <div class="row align-items-center justify-content-md-between">
          <div class="col-md-6">
            <div class="copyright">
              &copy;{{date("Y")}} <a href="#" target="_blank">WFWZ </a>.
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>


<!-- modal fade booking -->
  <!-- Modal -->
<div class="modal fade " id="booking" tabindex="-1" role="dialog" aria-labelledby="booking" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Book Your Date</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      <form action="{{route('public.book')}}" method="POST">
          @csrf
        <div class="modal-body">
          @error('clash')
          <div class="alert alert-danger text-center" >
            😱 &nbsp; {{$message}} &nbsp;🤙
          </div>
          @enderror
          <div class="row">
            <div class="col-lg-4">
              <label>Select Package</label>
            <select class="form-control @error('package') is-invalid @enderror" name="package" value="{{old('package')}}" >
                <option>--Select Package--</option>
            @forelse ($package as $item)
              <option value="{{$item->id}}"> {{$item->name}} &nbsp;{{$item->description}}</option>
            @empty
                
            @endforelse
              </select>
              @error('package')<span>{{$message}}</span> @enderror
            </div>
            <div class="col-lg-4">
              <label>Date</label>
              <input type="date" class="form-control @error('date') is-invalid @enderror" name="date">
              @error('date')<span>{{$message}}</span> @enderror
            </div>
            <div class="col-lg-4">
              <label>Time</label>
            <input type="time" class="form-control @error('time') is-invalid @enderror" min='15:00' max= '19:00' value="{{old('time')}}" name="time">
              @error('date')<span>{{$message}}</span> @enderror
            </div>
          </div>
          <div class="row" >
            <div class="col-md-12"> 
              <label>Name</label>
            <input type="text" class="form-control  @error('name') is-invalid @enderror"  name="name" value="{{old('name')}}">
            @error('name')<span>{{$message}}</span> @enderror
            </div>
          </div>
          <div class="row" >
            <div class="col-md-12"> 
              <label>Email</label>
              <input type="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{old('email')}}">
              @error('email')<span>{{$message}}</span> @enderror
            </div>
          </div>
          <div class="row" >
            <div class="col-md-12"> 
              <label>Phone</label>
              <input type="text" class="form-control  @error('phone') is-invalid @enderror" name="phone" value="{{old('phone')}}">
              @error('phone')<span>{{$message}}</span> @enderror
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" >Book </button>
        </div>
      </form>
      </div>
    </div>
  </div>

  <style>
    .card-img-top {
    width: 100%;
    height: 15vw;
    object-fit: fill;
}
  </style>
  @section('script')
  @if($errors->has('name')||$errors->has('phone')||$errors->has('email')||$errors->has('package')||$errors->has('date')||$errors->has('time')||$errors->has('clash'))
  <script>
      $('#booking').modal('show');
  </script>    
   @endif
  <script>
  $('.alert-success').fadeIn().delay(4000).fadeOut();


  </script>
      
  @endsection
@endsection