@extends('base.landing')
@section('content')
<link href="https://fonts.googleapis.com/css2?family=Hammersmith+One&display=swap" rel="stylesheet">
		<div id="wrap">
            @include('base.header')

			<main id="content" class="content">

				<section class="vc_row ">
					<div class="container">
						<div class="row mt-5">
							
							<div class="lqd-column col-md-12 text-center">

								<div class="lqd-column-inner border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">

									<div class="row d-flex flex-wrap align-items-center">
	
										<div class="lqd-column col-md-12 text-center">
                                            <h2 class="text-center"  style="font-family: 'Hammersmith One', sans-serif; font-style: italic;">" Come and Experience the Treasure of Terengganu"</h2>
										</div><!-- /.lqd-column col-md-4 -->
									</div><!-- /.row -->

								</div><!-- /.lqd-column-inner -->

							</div><!-- /.lqd-column col-md-12 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
                </section>
                
                {{-- //single rider --}}
                <form action="{{route('save.cancelbook')}}" method="POST">
                    @csrf
                <section class="vc_row pt-50 pb-50" >
					<div class="container border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">
                            <div class="row d-flex flex-wrap">
                                <div class="lqd-column col-sm-6 col-md-offset-5 text-center"><h4 class="font-weight-bold">Cancel Booking Page</h4>
                                </div>
                            </div>
						<div class="row d-flex flex-wrap align-items-center mb-3">

							<div class="lqd-column col-md-5  ">
                                <div class="liquid-img-group-single mb-3" data-reveal="true" data-reveal-options='{"direction":"lr","bgcolor":"","delay":""}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid">
										</div><!-- /.liquid-img-group-content --> 
										<div class="liquid-img-container-inner">
											<figure>
											<img width="1141" height="760" src="{{asset('assets/img/gambar/about.jpg')}}" alt="On your own, you see. On a tour, you do." /> 
											</figure> 
										</div><!-- /.liquid-img-container-inner -->
								 </div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single --> 


							</div><!-- /.lqd-column col-md-5 -->
                            <input type="hidden" name="booking_id" value="{{$booking->id}}">
 							<div class="lqd-column col-md-5 col-md-offset-1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control" value="{{$booking->customer_name}}" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Contact</label>
                                        <input type="text" name="phone" class="form-control" value="{{$booking->customer_contact}}" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" value="{{$booking->customer_email}}" required>
                                    </div>
                                </div>
                
                            <div class="row mt-5">
                                <div class="col-md-6 col-md-offset-6 text-center">
                                 <button class="btn btn-primary py-2 px-5">Cancel My Booking</button>
                                </div>
                            </div>

							

							</div><!-- /.lqd-column col-md-6 col-md-offset-1 -->

                        </div><!-- /.row -->
                      
					</div><!-- /.container -->
                </section> 
                </form>

                <section class="vc_row pt-50 pb-50" >
					<div class="container border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">
                            {{-- <div class="row d-flex flex-wrap">
                                <div class="lqd-column col-sm-6 col-md-offset-5 text-center"><h4 class="font-weight-bold">Double RIDER</h4>
                                    <p>Enjoy Couple Ride</p>
                                </div>
                            </div> --}}
						<div class="row d-flex flex-wrap align-items-center mb-3">

							<div class="lqd-column col-md-12">
                                <h2 class="text-left">Description</h2>
                                <ul>
                                    <li>20 minutes ride</li>
                                    <li> Life Jacket Provided</li>
                                    <li>Thursday till Sunday only available for booking </li>
                                </ul>
                                <h2 class="text-left">Condition</h2>
                                <ul>
                                    <li> Customer must be over 16 years old for pilot and 12 years above for passengers </li>
                                    <li>All bookings need to be a week earlier </li>
                                    <li>1 Jetski per person for people over 100kg</li>
                                    <li>It is required to arrive 20 minutes earlier. If the guest is not at the starting point 15 minutes before the scheduled time without notifiying a delay, the reservation will be canceled </li>
                                    <li>These are the mandatory requirements. We strongly invite you to also consult our advices in order to arrive without stress and spend the best moments</li>
                                    <li>Please inform us by telephone,sms or by e-mail in case of delay</li>
                                  
                                </ul>
                            </div>
                               
                        </div><!-- /.row -->
                      
					</div><!-- /.container -->
                </section> 


			</main><!-- /#content.content -->
@include('base.footer')

		</div><!-- /#wrap -->
@endsection
