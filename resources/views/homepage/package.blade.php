@extends('base.landing')
@section('content')
		<div id="wrap">
            @include('base.header')

			<main id="content" class="content">

				<section class="vc_row pb-90">
					<div class="container">
						<div class="row">
							
							<div class="lqd-column col-md-12 pull-up-05x text-center">

								<div class="lqd-column-inner border-radius-3 bg-white column-shadowed-1 px-3 px-md-5 pt-35 pb-35">

									<div class="row d-flex flex-wrap align-items-center">
	
										<div class="lqd-column col-md-4 text-md-left">
											<h6 class="my-md-0 text-primary">Jetski Route ></h6>
										</div><!-- /.lqd-column col-md-4 -->
	
										<div class="lqd-column col-md-8 text-md-right">
											<ul class="lqd-custom-menu reset-ul inline-nav mb-md-0">
												<li><a href="#">Kuala Terengganu Draw Bridge</a></li>
												<li><a href="#">Taman Tamadun Islam</a></li>
												<li><a href="#">Duyung Marina Bay</a></li>
												<li><a href="#">Pulau Warisan</a></li>
											</ul>
										</div><!-- /.lqd-column col-md-8 -->
	
									</div><!-- /.row -->

								</div><!-- /.lqd-column-inner -->

							</div><!-- /.lqd-column col-md-12 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
				</section>

                @forelse ($package as $item)
				<section class="vc_row pt-50 pb-50 shadow p-3 mb-5 bg-white rounded" >
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center" >

							<div class="lqd-column col-md-5 mb-30" >

                            <h2 class="mt-0 mb-3" >{{$item->name}}</h2>
								<!-- <h3 class="mt-0 mb-40 font-size-14 text-uppercase ltr-sp-05 text-secondary">Package Name</h3> -->
								<div class="lqd-h-sep w-10 mb-40">
									<span class="lqd-h-sep-inner"></span><!-- /.lqd-h-sep-inner -->
								</div><!-- /.lqd-h-sep -->
								<p class="font-weight-bold text-uppercase" style="color: black"> Description</p>
                            <p class="text-capitalize" >{{ $item->description}}<br> RM {{$item->price}} For {{$item->duration}} Minutes</p>
								<br>

                            <a href="{{route('single.package',$item->id)}}" class="btn btn-naked font-weight-bold text-uppercase">
									<span>
										<span class="btn-txt ">Book This Package</span>
									</span>
								</a>

							</div><!-- /.lqd-column col-md-5 -->

							<div class="lqd-column col-md-6 col-md-offset-1 text-center mb-30">

								<div class="liquid-img-group-single mb-3" data-reveal="true" data-reveal-options='{"direction":"lr","bgcolor":"","delay":""}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid">
										</div><!-- /.liquid-img-group-content -->
										<div class="liquid-img-container-inner">
											<figure>
                                            <img width="1141" height="760" src="{{$item->image}}" style="width: 70%;heght:70%;"/>
											</figure>
										</div><!-- /.liquid-img-container-inner -->
									</div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single -->

								{{-- <p>Ride On Your Own Or We Bring You For The Ride</p> --}}

							</div><!-- /.lqd-column col-md-6 col-md-offset-1 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
                </section>
                <hr>
                @empty
                    
                @endforelse
				


			</main><!-- /#content.content -->
@include('base.footer')

		</div><!-- /#wrap -->
@endsection
