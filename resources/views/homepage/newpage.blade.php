@extends('base.landing')
@section('content')

<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@900&display=swap" rel="stylesheet">
<style>
	/* .header-font{ */
		/* font-family: 'Roboto', sans-serif; */
	/* } */
</style>
	<div id="wrap">
            @include('base.header')
			<main id="content" class="content">
				<section class="vc_row bg-cover fullheight d-flex align-items-center py-8" data-parallax="true"
					data-parallax-options='{"parallaxBG": true}' data-row-bg="{{asset('assets/img/gambar/1.jpg')}}">

					<span class="row-bg-loader"></span>

					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">

							<div
							class="lqd-column col-sm-8 col-sm-offset-2 text-center mt-sm-9"
							data-custom-animations="true"
							data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1400,"delay":"200","startDelay": 250,"easing":"easeOutQuint","direction":"forward","initValues":{"translateY":45,"opacity":0},"animations":{"translateY":0,"opacity":1}}'>

								<div class="ld-fancy-heading text-uppercase ltr-sp-2">
									<h6
									class="text-white"
									data-split-text="true"
									data-split-options='{"type":"words"}'>
										<span class="ld-fh-txt header-font" style="font-size: 30px;"> 1# Watersport In Terengganu </span>
									</h6>
								</div><!-- /.ld-fancy-heading -->

								<figure class="mb-30">
									<h1 style="color: white; " class="header-font">JETSKI TOUR ADVENTURE</h1>
								</figure>

								<div class="ld-fancy-heading mb-4">
									<p class="font-size-18 text-white">
										{{-- <span class="ld-fh-txt header-font"> The Perfect Watersport Experience </span> --}}
									</p>
								</div><!-- /.ld-fancy-heading -->

                            <a href="{{url('/package')}}" class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-2">
									<span>
										<span class="btn-txt">EXPLORE</span>
									</span>
								</a>

							</div><!-- /.lqd-column col-md-8 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
				</section>
				{{-- pulau kapas section --}}
				<section class="vc_row bg-cover fullheight d-flex align-items-center py-8" data-parallax="true"
					data-parallax-options='{"parallaxBG": true}' data-row-bg="{{asset('assets/img/map.png')}}">

					<span class="row-bg-loader"></span>

					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">

							<div
							class="lqd-column col-md-8 col-md-offset-2 text-center mt-md-9"
							data-custom-animations="true"
							data-ca-options='{"triggerHandler":"inview","animationTarget":"all-childs","duration":1400,"delay":"200","startDelay": 250,"easing":"easeOutQuint","direction":"forward","initValues":{"translateY":45,"opacity":0},"animations":{"translateY":0,"opacity":1}}'>

								<div class="ld-fancy-heading text-uppercase ltr-sp-2">
									<h6
									class="text-white"
									data-split-text="true"
									data-split-options='{"type":"words"}'>
										<h2 class="header-font">JETSKI ROUTE</h2>
									</h6>
								</div><!-- /.ld-fancy-heading -->

								<figure class="mb-30">
									<h2 class="header-font">Explore Terengganu Iconic Places</h2>
								</figure>
							</div><!-- /.lqd-column col-md-8 -->
						</div><!-- /.row -->
					</div><!-- /.container -->
				</section>
				{{-- activities --}}
				
				<section class="vc_row pt-50 pb-50" id="about">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">
							<div class="lqd-column col-md-12 mb-30">
								<h2 class="text-center" >ACTIVITIES</h2>
								<div class="row">
									<div class="col-sm-6">
										{{-- <h4 style="z-index:2;">Best Seller !!!</h4> --}}
										<div class="fancy-box content-box-heading-sm fancy-box-tour">
											<figure class="fancy-box-image loaded">
											<img width="564" height="500" alt="" src="{{asset('gambar/1.jpg')}}">
											</figure>
											<div class="fancy-box-contents">
												<div class="fancy-box-header text-center">
													<a href=""><h3 class="font-weight-semibold text-uppercase">Single rider</h3></a>
												</div><!-- /.fancy-box-header -->
												<div class="fancy-box-info text-center">
												<button class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-3 py-2">Book A Ride</button>
												</div><!-- /.fancy-box-info -->
											</div><!-- /.fancy-box-contents -->
										</div><!-- /.fancy-box -->
									</div>
									<div class="col-sm-6">
										<div class="fancy-box content-box-heading-sm fancy-box-tour">
											
											<figure class="fancy-box-image loaded">
											<img width="564" height="500" alt="" src="{{asset('gambar/1.jpg')}}">
											</figure>
											<div class="fancy-box-contents">
												<div class="fancy-box-header text-center">
													<a href=""><h3 class="font-weight-semibold text-uppercase">Double rider</h3></a>
												</div><!-- /.fancy-box-header -->
												<div class="fancy-box-info text-center">
													<button class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-3 py-2">Book A Ride</button>
												</div><!-- /.fancy-box-info -->
											</div><!-- /.fancy-box-contents -->
										</div><!-- /.fancy-box -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>

				<section class="vc_row pt-50 pb-50"  style="background-image: url({{asset('assets/img/gambar/kapas.png')}});  background-repeat: no-repeat; background-size: cover;">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">
							<div class="lqd-column col-md-12 mb-30">
								<h2 class="text-center text-white" >Special Trip !!!</h2>
								<h2 class="text-center text-white" >EYE CATCHING OF KAPAS ISLAND </h2>
								<div class="row">
									<div class="col-sm-12">
										<div class="text-center">
											<h4 class="text-white">LOVE TO EXPLORE CORAL REEF UNDERWATER ?? </h4>
											<h4 class="text-white">YOU ALSO CAN CATCH OUT THE WONDERFUL MARINE LIFE</h4>
											<div class="text-center">
												<button class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-4 py-2">Book A Ride</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</section>
{{-- 
				<section class="vc_row pt-50 pb-50" id="about">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">

							<div class="lqd-column col-md-5 mb-30">

								<h2 class="mt-0 mb-3" >About WFWZ Watersports</h2>
								<h3 class="mt-0 mb-40 font-size-14 text-uppercase ltr-sp-05 text-secondary">First Watersport in Kuala Terengganu</h3>

								<div class="lqd-h-sep w-10 mb-40">
									<span class="lqd-h-sep-inner"></span><!-- /.lqd-h-sep-inner -->
								</div><!-- /.lqd-h-sep -->

								<p> We have a selection of over 50 tour programs that range from introductory multi-country itineraries to more regional in-depth options.</p>
								<br>
								<p>You’re about to go on the most amazing journey! Welcome to one of Europe’s leading coach tour operators, Ave Tour.</p>
								<br>

								{{-- <a href="#" class="btn btn-naked font-weight-bold text-uppercase">
									<span>
										<span class="btn-txt">Learn More</span>
									</span>
								</a> 

							</div><!-- /.lqd-column col-md-5 -->

							<div class="lqd-column col-md-6 col-md-offset-1 text-center mb-30">

								{{-- <div class="liquid-img-group-single mb-3" data-reveal="true" data-reveal-options='{"direction":"lr","bgcolor":"","delay":""}'>
									<div class="liquid-img-group-img-container">
										<div class="liquid-img-group-content content-floated-mid">
										</div><!-- /.liquid-img-group-content --> 
										<div class="liquid-img-container-inner">
											<figure>
												{{-- <img width="1141" height="760" src="{{asset('assets/img/gambar/about.jpg')}}" alt="On your own, you see. On a tour, you do." /> 
												<video controls height="700px;">
													<source src="{{asset('gambar/1.mp4')}}" type="video/mp4">
												</video>
											</figure> 
										</div><!-- /.liquid-img-container-inner -->
									{{-- </div><!-- /.liquid-img-group-img-container -->
								</div><!-- /.liquid-img-group-single --> 

								<p>Ride On Your Own Or We Bring You For The Ride</p>

							</div><!-- /.lqd-column col-md-6 col-md-offset-1 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
				</section> --}}

				<section class="vc_row pt-90 pb-90 mb-50 mt-50 ">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">

							<div class="lqd-column col-md-12">

								<header class="lqd-fancy-title mb-md-0 text-center">
									<h2 class="my-0 text-uppercase ">Our Gallery</h2>
								</header><!-- /.lqd-fancy-title -->

							</div><!-- /.lqd-column col-md-8 -->
								<div class="lqd-column col-md-12 mt-30">
								<div class="carousel-container carousel-nav-floated carousel-nav-center carousel-nav-middle carousel-nav-md carousel-nav-solid carousel-nav-rectangle carousel-nav-shadowed carousel-dots-style1">
									<div class="carousel-items row mx-0" data-lqd-flickity='{"cellAlign":"center","prevNextButtons":true,"buttonsAppendTo":"self","pageDots":false,"groupCells":true,"wrapAround":true,"pauseAutoPlayOnHover":false,"navArrow":"1","navOffsets":{"nav":{"top":"25%"},"prev":"-13px","next":"-13px"}}'>
										{{-- @forelse ($package as $item) --}}
                                        <div class="carousel-item col-sm-8 col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/gambar/12.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm-8 col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/gambar/12.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm-8 col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/gambar/12.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm-8 col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/gambar/12.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm-8 col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/gambar/12.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
                                        <div class="carousel-item col-sm- col-md-3 px-1">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="600" height="500" src="{{asset('assets/img/gambar/12.jpg')}}">
												</figure>
										</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-8 col-md-4 -->
									</div><!-- /.carousel-items -->
								</div><!-- /.carousel-container -->

							</div><!-- /.lqd-column col-md-12 -->

						</div><!-- /.row d-flex flex-wrap align-items-center -->
					</div><!-- /.container -->
				</section>
				{{-- how to find us --}}
				<section class="vc_row pt-90 pb-90 mb-50 mt-50 ">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">

							<div class="lqd-column col-md-12">

								<header class="lqd-fancy-title mb-md-0 text-center">
									<h2 class="my-0 text-uppercase ">How To Find Us ?</h2>
								</header><!-- /.lqd-fancy-title -->
								
							</div>
						</div>
							<div class="row">
								<div class="lqd-column col-md-12 p-5">
									<header class="lqd-fancy-title mb-md-0 text-center">
								<a href="#" class="btn btn-solid text-uppercase semi-round btn-bordered border-thin px-4 py-2">
									Book a ride today
								</a>
									</header>
								</div>
							</div>
						</div>
					
				</section>



				{{-- <section class="vc_row pt-90 pb-90 mb-50 mt-50 ">
					<div class="container">
						<div class="row d-flex flex-wrap align-items-center">

							<div class="lqd-column col-md-8">

								<header class="lqd-fancy-title mb-md-0">
									<h2 class="my-0 text-uppercase">Our Package</h2>
									<p>Check these awesome adventures off your bucket list!</p>
								</header><!-- /.lqd-fancy-title -->

							</div><!-- /.lqd-column col-md-8 -->

							<div class="lqd-column col-md-4 text-md-right">

                            <a href="{{route('public.package')}}" class="btn btn-naked font-weight-bold text-uppercase">
									<span>
										<span class="btn-txt">See all package</span>
									</span>
								</a>

							</div><!-- /.lqd-column col-md-4 -->

							<div class="lqd-column col-md-12 mt-30">

								<div class="carousel-container carousel-nav-floated carousel-nav-center carousel-nav-middle carousel-nav-md carousel-nav-solid carousel-nav-rectangle carousel-nav-shadowed carousel-dots-style1">
									<div class="carousel-items row mx-0" data-lqd-flickity='{"cellAlign":"center","prevNextButtons":true,"buttonsAppendTo":"self","pageDots":false,"groupCells":true,"wrapAround":true,"pauseAutoPlayOnHover":false,"navArrow":"1","navOffsets":{"nav":{"top":"25%"},"prev":"-13px","next":"-13px"}}'>
										@forelse ($package as $item)
                                        <div class="carousel-item col-sm-6 col-md-3 px-2">
											<div class="fancy-box content-box-heading-sm fancy-box-tour">
												<figure class="fancy-box-image loaded">
                                                <img width="564" height="500" src="{{$item->image}}" alt="{{$item->name}}">
												</figure>
												<div class="fancy-box-contents">
													<div class="fancy-box-header">
														<a href="{{route('single.package',$item->id)}}"><h3 class="font-weight-semibold text-uppercase">{{$item->name}}</h3></a>
														<div class="rating">
															<ul class="star-rating">
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
															</ul> 
														</div><!-- /.rating -->
													</div><!-- /.fancy-box-header -->
													<div class="fancy-box-info">
                                                    <p>{{$item->description}}</p>
													</div><!-- /.fancy-box-info -->
												</div><!-- /.fancy-box-contents -->
												<div class="fancy-box-footer">
                                                <h6> From <strong>RM {{$item->price}}</strong> </h6>
													<span class="fancy-box-icon"><i class="icon-liquid_arrow_right"></i></span>
												</div><!-- /.fancy-box-footer -->
											</div><!-- /.fancy-box -->
										</div><!-- /.carousel-item col-sm-6 col-md-4 -->
                                        @empty
                                            
                                        @endforelse

									</div><!-- /.carousel-items -->
								</div><!-- /.carousel-container -->

							</div><!-- /.lqd-column col-md-12 -->

						</div><!-- /.row d-flex flex-wrap align-items-center -->
					</div><!-- /.container -->
				</section>

				<section class="vc_row pt-90 pb-90 mt-40 mb-40" style="background-color: #0c3e72;">
					<div class="container">
						<div class="row d-md-flex flex-wrap align-items-center">

							<div class="lqd-column col-md-12 mb-40">

								<header class="fancy-title mb-25" id="travel-testimonials-title">
									<h2 class="text-white text-uppercase ltr-sp-0">Customer Reviews</h2>
								</header><!-- /.fancy-title underlined -->

								<hr class="bt-fade-white-01">

							</div><!-- /.lqd-column col-md-12 -->

							<div class="lqd-column col-md-3 mb-30 mb-md-0">
								<div class="lqd-column-inner px-4 pt-35 pb-35" style="background-color: #0a345f;">

									<h3 class="text-white mt-0 mb-3">Excellent</h3>
									<img class="mb-4" src="./assets/demo/misc/star-rating.svg" alt="Star Rating">
									<p class="text-fade-white-05">Our Previous Customer Review</p>
									<!-- <img src="./assets/demo/misc/logo-5.png" alt="Trustpilot"> -->

								</div><!-- /.lqd-column-inner -->
							</div><!-- /.lqd-column col-md-3 -->

							<div class="lqd-column col-md-9 pt-md-2">

								<div class="carousel-container carousel-nav-right carousel-nav-md carousel-nav-solid carousel-nav-rectangle carousel-nav-shadowed-onhover carousel-dots-style1">

									<div class="carousel-items row" data-lqd-flickity='{"cellAlign": "center","prevNextButtons": true,"buttonsAppendTo": "#travel-testimonials-title","pageDots": false,"groupCells": true,"wrapAround": true,"pauseAutoPlayOnHover": false}'>

										<div class="carousel-item col-sm-6 col-xs-12 px-md-7">

											<div class="testimonial testimonial-sm text-left testimonial-details-sm">
												<div class="testimonial-details">
													<ul class="star-rating square sm">
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
													</ul>
													<!-- <time datetime="2019-09-18">3 hours ago</time> -->
												</div><!-- /.testimonial-details -->
												<div class="testimonial-quote">
													<blockquote>
														<h6 class="text-white">First Time Ride</h6>
														<p class="font-size-13 lh-185 text-fade-white-06">This is my first time riding jetski normally i just look at other ride it </p>
													</blockquote>
												</div><!-- /.testimonial-quote -->
												<div class="testimonial-details">
													<div class="testimonial-info">
														<h5 class="text-white">Customer 1</h5>
													</div><!-- /.testimonial-info -->
												</div><!-- /.testimonial-details -->
											</div><!-- /.testimonial -->

										</div><!-- /.carousel-item col-sm-6 col-xs-12 -->

										<div class="carousel-item col-sm-6 col-xs-12 px-md-7">

											<div class="testimonial testimonial-sm text-left testimonial-details-sm">
												<div class="testimonial-details">
													<ul class="star-rating square sm">
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
													</ul>
													<!-- <time datetime="2019-09-18">3 hours ago</time> -->
												</div><!-- /.testimonial-details -->
												<div class="testimonial-quote">
													<blockquote>
														<h6 class="text-white">Sangat Berpuas hati</h6>
														<p class="font-size-13 lh-185 text-fade-white-06">Saya sebenarnya tak reti sangat nak bawa jetski Tapi!! Selamat WFWZ ada boleh bawakan untuk Saya
														</p>
													</blockquote>
												</div><!-- /.testimonial-quote -->
												<div class="testimonial-details">
													<div class="testimonial-info">
														<h5 class="text-white">Mohd Azim </h5>
													</div><!-- /.testimonial-info -->
												</div><!-- /.testimonial-details -->
											</div><!-- /.testimonial -->

										</div><!-- /.carousel-item col-sm-6 col-xs-12 -->

										<div class="carousel-item col-sm-6 col-xs-12 px-md-7">

											<div class="testimonial testimonial-sm text-left testimonial-details-sm">
												<div class="testimonial-details">
													<ul class="star-rating square sm">
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
													</ul>
													
												</div><!-- /.testimonial-details -->
												<div class="testimonial-quote">
													<blockquote>
														<h6 class="text-white">Merakam Memori</h6>
														<p class="font-size-13 lh-185 text-fade-white-06">Selain membawa jetski saya juga melanggan dron daripada WFWZ untuk merakam video saya membawa jetski</p>
													</blockquote>
												</div><!-- /.testimonial-quote -->
												<div class="testimonial-details">
													<div class="testimonial-info">
														<h5 class="text-white">Anis</h5>
													</div><!-- /.testimonial-info -->
												</div><!-- /.testimonial-details -->
											</div><!-- /.testimonial -->

										</div><!-- /.carousel-item col-sm-6 col-xs-12 -->

										<div class="carousel-item col-sm-6 col-xs-12 px-md-7">

											<div class="testimonial testimonial-sm text-left testimonial-details-sm">
												<div class="testimonial-details">
													<ul class="star-rating square sm">
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
														<li><i class="fa fa-star"></i></li>
													</ul>
													
												</div><!-- /.testimonial-details -->
												<div class="testimonial-quote">
													<blockquote>
														<h6 class="text-white">Great tour </h6>
														<p class="font-size-13 lh-185 text-fade-white-06">We just got back from our best vacation ever and we wanted to thank wfwz team for your excellent work…</p>
													</blockquote>
												</div><!-- /.testimonial-quote -->
												<div class="testimonial-details">
													<div class="testimonial-info">
														<h5 class="text-white">Customer 3</h5>
													</div><!-- /.testimonial-info -->
												</div><!-- /.testimonial-details -->
											</div><!-- /.testimonial -->

										</div><!-- /.carousel-item col-sm-6 col-xs-12 -->

									</div><!-- /.carousel-items -->

								</div><!-- /.carousel-contaienr -->

							</div><!-- /.lqd-column col-md-9 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
				</section> --}}

			</main><!-- /#content.content -->

            @include('base.footer')

        </div><!-- /#wrap -->
        
        @endsection
