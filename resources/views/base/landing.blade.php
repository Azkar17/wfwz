<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="theme-color" content="#3ed2a7">

		<link rel="shortcut icon" href="./logo.png" />

		<title>WFWZ Watersport</title>

		<link href="https://fonts.googleapis.com/css?family=Muli:400,500%7cQuicksand:400,700&display=swap" rel="stylesheet">

		<link rel="stylesheet" href="{{asset('assets/vendors/liquid-icon/liquid-icon.min.css')}}" />
		<link rel="stylesheet" href="{{asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}" />
		<link rel="stylesheet" href="{{asset('assets/css/theme-vendors.min.css')}}" />
		<link rel="stylesheet" href="{{asset('assets/css/theme.min.css')}}" />
		<link rel="stylesheet" href="{{asset('assets/css/themes/travel.css')}}" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.13.3/sweetalert2.css" integrity="sha512-tAB44Mx++ci+FGvwu3N7f1RSXeN2s+M+pbJHHbYOmkla05H3zV0Y/LFVtwFAPU92HQcLj6SRHS925pcBBxJ4XQ==" crossorigin="anonymous" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.13.3/sweetalert2.min.js" integrity="sha512-9FvknuL9tC/meU7bZ0UwSMS7mI9pwepLMdbuP2seYGgmPhgiUnSSp8iAXKvciWHU86cEhzvHiXMVPHH0QXO9Uw==" crossorigin="anonymous"></script>

		<!-- Head Libs -->
		<script async src="{{asset('assets/vendors/modernizr.min.js')}}"></script>

	</head>

	<body data-mobile-nav-trigger-alignment="right" data-mobile-nav-align="left" data-mobile-nav-style="modern"
        data-mobile-nav-shceme="gray" data-mobile-header-scheme="gray" data-mobile-nav-breakpoint="1199">
        
    @yield('content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
		<script src="{{asset('assets/js/theme-vendors.js')}}"></script>
		<script src="{{asset('assets/js/theme.min.js')}}"></script>
		<script src="{{asset('assets/js/liquidAjaxMailchimp.min.js')}}"></script>
    @yield('script')

	</body>

</html>