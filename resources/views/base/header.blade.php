<header class="main-header main-header-overlay bg-white">
    <div class="mainbar-wrap">
        <div class="megamenu-hover-bg"></div><!-- /.megamenu-hover-bg -->
        <div class="container-fluid mainbar-container">
            <div class="mainbar">
                <div class="row mainbar-row align-items-lg-stretch px-4">

                    <div class="col-auto">
                        <div class="navbar-header">
                            <a  href="{{route('home')}}" rel="home">
                                {{-- <span class="navbar-brand-inner"> --}}
                                    {{-- <img class="logo-dark"style="height:80px;" src="{{asset('assets/img/logo/logo.jpeg')}}" >
                                    <img class="logo-light"style="height:80px;" src="{{asset('assets/img/logo/logo.jpeg')}}" >
                                    <img class="logo-sticky"style="height:80px;" src="{{asset('assets/img/logo/logo.jpeg')}}" >
                                    <img class="mobile-logo-default"style="height:80px;" src="{{asset('assets/img/logo/logo.jpeg')}}" > --}}
                                    
                                {{-- </span> --}}
                                <img class="logo-default"style="height:125px; " src="{{asset('assets/img/logo/logo.jpeg')}}" >
                            </a>
                            <button type="button" class="navbar-toggle collapsed nav-trigger style-mobile"
                                data-toggle="collapse" data-target="#main-header-collapse" aria-expanded="false"
                                data-changeclassnames='{ "html": "mobile-nav-activated overflow-hidden" }'>
                                <span class="sr-only">Toggle navigation</span>
                                <span class="bars">
                                    <span class="bar"></span>
                                    <span class="bar"></span>
                                    <span class="bar"></span>
                                </span>
                            </button>
                        </div><!-- /.navbar-header -->
                    </div><!-- /.col -->

                    <div class="col px-8">

                        <div class="collapse navbar-collapse" id="main-header-collapse">

                            <ul id="primary-nav" class="main-nav main-nav-hover-underline-3 nav align-items-lg-stretch justify-content-lg-start" data-submenu-options='{ "toggleType":"fade", "handler":"mouse-in-out" }'>

                                <li class="{{ Request::is('/') ? 'current-menu-item' : '' }}">
                                    <a href="{{url('/')}}">
                                        <span class="link-icon"></span>
                                        <span class="link-txt">
                                            <span class="link-ext"></span>
                                            <span class="txt text-uppercase">
                                                Home {{Request::is()}}
                                                <span class="submenu-expander">
                                                    <i class="fa fa-angle-down"></i>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li  class="{{ Request::is('/packagenew') ? current-menu-item : '' }}">
                                    <a href="{{url('/packagenew')}}">
                                        <span class="link-icon"></span>
                                        <span class="link-txt">
                                            <span class="link-ext"></span>
                                            <span class="txt text-uppercase">
                                                Package
                                                <span class="submenu-expander">
                                                    <i class="fa fa-angle-down"></i>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                           

                            </ul><!-- /#primary-nav  -->

                        </div><!-- /#main-header-collapse -->

                    </div><!-- /.col -->

                    <div class="col text-right">

                        <div class="header-module">
                            <div class="iconbox iconbox-side iconbox-heading-xs iconbox-sm">

                                <h3 class="p-2">016 341 9366</h3> 
                            </div><!-- /.iconbox -->
                        </div><!-- /.header-module --> 
                        <div class="header-module">
                            <ul class="social-icon branded-text social-icon-md">
                                <li><a href="https://www.facebook.com/sewajetski" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                {{-- <li><a href="#"><i class="fa fa-twitter"></i></a></li> --}}
                                {{-- <li><a href="#"><i class="fa fa-pinterest"></i></a></li> --}}
                                <li><a href="https://instagram.com/wfwzinsight?igshid=1dyl6gmcaxz6l" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="https://watsap.my/60163419366" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                            </ul>
                        </div><!-- /.header-module -->

                        {{-- <div class="header-module">
                            <div class="iconbox iconbox-side iconbox-heading-xs iconbox-sm">
                                <div class="iconbox-icon-wrap">
                                    <span class="iconbox-icon-container">
                                        <i class="icon-ion-ios-call"></i> 
                                    </span>
                                </div><!-- /.iconbox-icon-wrap -->
                                <h3>016 341 9366</h3> 
                            </div><!-- /.iconbox -->
                        </div><!-- /.header-module --> --}}

                    </div><!-- /.col -->

                </div><!-- /.mainbar-row -->
            </div><!-- /.mainbar -->
        </div><!-- /.mainbar-container -->
    </div><!-- /.mainbar-wrap -->

</header><!-- /.main-header -->