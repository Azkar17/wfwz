
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />

  <title>
   WFWZ watersport
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <!-- Nucleo Icons -->
  <link href="{{asset('argon/css/nucleo-icons.css')}}" rel="stylesheet" />
  <link href="{{asset('argon/css/nucleo-svg.css')}}" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <link href="{{asset('argon/css/font-awesome.css')}}" rel="stylesheet" />
  <link href="{{asset('argon/css/nucleo-svg.css')}}" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="{{asset('argon/css/argon-design-system.css?v=1.2.0')}}" rel="stylesheet" />
</head>

<body class="landing-page">

    @yield('content')
 


  <!--   Core JS Files   -->
  <script src="{{asset('argon/js/core/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('argon/js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('argon/js/core/bootstrap.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('argon/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="{{asset('argon/js/plugins/bootstrap-switch.js')}}"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{asset('argon/js/plugins/nouislider.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('argon/js/plugins/moment.min.js')}}"></script>
  <script src="{{asset('argon/js/plugins/datetimepicker.js')}}" type="text/javascript"></script>
  <script src="{{asset('argon/js/plugins/bootstrap-datepicker.min.js')}}"></script>
  <!-- Control Center for Argon UI Kit: parallax effects, scripts for the example pages etc -->
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <script src="{{asset('argon/js/argon-design-system.min.js?v=1.2.0')}}" type="text/javascript"></script>
  @yield('script')
</body>

</html>