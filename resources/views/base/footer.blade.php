
			<footer class="main-footer pt-100 pb-100">

				<section class="vc_row">
					<div class="container">
						<div class="row text-center">

							<div class="lqd-column col-md-6 col-xs-6 mb-30">

								<h3 class="widget-title text-uppercase">WFWZ Insight Jetski Dropping Point</h3>
								<ul class="lqd-custom-menu reset-ul font-size-13 lh-2">
									<li><a href="#">Pasar Payang</a></li>
									<li><a href="#">Jeti Pulau Duyung</a></li>
									<li><a href="#">Pulau Warisan</a></li>
									<li><a href="#">Belakang KTCC Mall</a></li>
								</ul>
							</div><!-- /.col-md-2 -->

							<div class="lqd-column col-md-6 col-xs-6 mb-30 ">

								<h3 class="widget-title text-uppercase">Jetski Route</h3>

								<div class="row text-center">
									<div class="lqd-column col-md-12">
										<ul class="lqd-custom-menu reset-ul font-size-13 lh-2">
											<li><a href="#">Kuala Terengganu</a></li>
											<li><a href="#">Taman Tamadun Islam</a></li>
											<li><a href="#">Duyung Marina Bay</a></li>
											<li><a href="#">Pulau Warisan</a></li>
										</ul>
									</div><!-- /.lqd-column col-md-6 -->
									 <div class="lqd-column col-md-6"> 

										
									</div><!-- /.lqd-column col-md-6 --> 
								</div><!-- /.row -->

							</div><!-- /.col-md-4 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
				</section>

				<section class="vc_row pt-60">
					<div class="container">
						<div class="row d-md-flex flex-wrap align-items-center">

							<div class="lqd-column col-md-9 mb-15 text-right">

								<figure class="mb-20">
									{{-- <img src="{{asset('assets/demo/misc/travel-aware-logo.jpg')}}" alt="Travel Aware Logo"> --}}
								</figure>

								<ul class="lqd-custom-menu reset-ul inline-nav font-size-13">
									<li><a href="#">Booking Terms</a></li>
									<li><a href="#">Cookies</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Website Terms</a></li>
									<li><a href="#">Book with Confidence</a></li>
									{{-- <li><a href="#">Foreign Office Travel Advice</a></li> --}}
								</ul>

							</div><!-- /.lqd-column col-md-9 -->

							<div class="lqd-column col-md-3 text-center text-md-right mb-3 mb-md-0">
								
								<ul class="social-icon branded circle social-icon-sm">

									<li><a href="https://www.facebook.com/sewajetski" target="_blank"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://instagram.com/wfwzinsight?igshid=1dyl6gmcaxz6l" target="_blank"><i class="fa fa-instagram"></i></a></li>
								</ul>

							</div><!-- /.lqd-column col-md-3 -->

							<div class="lqd-column col-md-12 text-center ">
								<p class="font-size-12">Copyright 2021 WFWZ Insight. All Rights Reserved.</p>
							</div><!-- /.lqd-column col-md-12 -->

						</div><!-- /.row -->
					</div><!-- /.container -->
				</section><!-- /.vc_row -->

			</footer><!-- /.main-footer -->