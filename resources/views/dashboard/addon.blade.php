@extends('base.dashboard-layout')
 @section('content')


 <div class="wrapper">
    <!-- Sidebar  -->
   
    @include('dashboard.sidebar')
    <!-- Page Content  -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="btn btn-dark">
                    <i class="fas fa-align-left"></i>
                   
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

 
            </div>
        </nav>

        <h2>Add on</h2>
        @if (Session::has('message'))
        <div class="alert alert-success">{{ Session::get('message') }}</div>
        @endif
        <div class="table-responsive py-2">
            <button class="btn btn-primary float-right" data-toggle="modal" data-target="#Package">Create</button>
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>image</th>
                  <th>title</th>
                  <th>price</th>
                  <th>description</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
               
            @forelse ($addons as $item)
                <tr>
                <td>{{$item->id}}</td>
               <td><img src="{{$item->image}}" class="img-fluid rounded-circle" style="height: 150px; width: 150px;"></td>
                  <td>{{$item->name}}</td>
                <td>{{$item->price}}</td>
                  <td>{{$item->description}}</td>
                  
                <td>   <button class="btn  btn-danger btn-sm delete" data-id="{{$item->id}}">Delete </button>
                  <button class="btn  btn-warning btn-sm edit" data-id="{{$item->id}}">Edit</button></td>
                </tr>
                @empty
                    
                @endforelse 
              </tbody>
            </table>
       
    </div>
</div>
 </div>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="Package" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Package</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    <form method="post" action="{{route('store.addons')}}"  enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
         <div class="row">
            <div class="col-lg-4 px-3">
                <div class="row my-2">
                    <div class="col-lg text-center">
                        <img src="https://dummyimage.com/250/ffffff/000000" class="img-fluid rounded-circle" style="height: 150px; width: 150px;">
                    </div>
                    </div>
                <div class="row my-2">
                <div class="col-md">
                    <input type="file" name="image" class="form-control px-2">
                </div>
                </div>
            </div>
            <div class="col-lg-8 px-3">
                <div class="row my-2">
                    <div class="col-lg">
                    <label>Name</label>
                    <input type="text" name="name"  class="form-control @error('name') is-invalid @enderror" value="{{old('name')}}">
                    <span>@error('name') {{$message}} @enderror</span>
                </div>
                </div>
                <div class="row my-2">
                    <div class="col-lg">
                    <label>Price</label>
                    <input type="text"  name="price" class="form-control @error('price') is-invalid @enderror" value="{{old('price')}}">
                    <span>@error('price') {{$message}} @enderror</span>
                </div>
                </div>
                <div class="row my-2">
                    <div class="col-lg">
                    <label>Description</label>
                    <textarea rows="3"  name="description" class="form-control @error('description') is-invalid @enderror">{{old('description')}}</textarea>
                   <span>@error('description') {{$message}} @enderror</span>
                </div>
                </div>
            </div>
         </div>
        </div>
        <div class="modal-footer">
          <button  class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary"  >Save </button>
        </div>
    </form>
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-lg" id="AddonEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create Package</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
    <form method="post" action="{{route('update.addons')}}"  enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
         <div class="row">
            <div class="col-lg-4 px-3">
                <div class="row my-2">
                    <div class="col-lg text-center">
                        <img src="https://dummyimage.com/250/ffffff/000000" class="img-fluid rounded-circle addon_image" style="height: 150px; width: 150px;">
                    </div>
                    </div>
                    <input type="hidden" name="id" class="addon_id">
                <div class="row my-2">
                <div class="col-md">
                    <input type="file" name="image" class="form-control px-2 " >
                </div>
                </div>
            </div>
            <div class="col-lg-8 px-3">
                <div class="row my-2">
                    <div class="col-lg">
                    <label>Name</label>
                    <input type="text" name="edit_name"  class="form-control addon_name @error('edit_name') is-invalid @enderror" value="{{old('edit_name')}}">
                    <span>@error('edit_name') {{$message}} @enderror</span>
                </div>
                </div>
                <div class="row my-2">
                    <div class="col-lg">
                    <label>Price</label>
                    <input type="text"  name="edit_price" class="form-control addon_price @error('edit_price') is-invalid @enderror" value="{{old('edit_price')}}">
                    <span>@error('edit_price') {{$message}} @enderror</span>
                </div>
                </div>
                <div class="row my-2">
                    <div class="col-lg">
                    <label>Description</label>
                   <textarea rows="3"  name="edit_description" class="form-control addon_description  @error('edit_description') is-invalid @enderror">{{old('edit_description')}}</textarea>
                   <span>@error('edit_description') {{$message}} @enderror</span>
                </div>
                </div>
            </div>
         </div>
        </div>
        <div class="modal-footer">
          <button  class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary "  >Update </button>
        </div>
    </form>
      </div>
    </div>
  </div>

 @section('script')



 @section('script')
 @if($errors->has('name')||$errors->has('price')||$errors->has('description'))
 <script>
     $('#Package').modal('show');
 </script>    
  @endif
 @if($errors->has('edit_name')||$errors->has('edit_price')||$errors->has('edit_description'))
 <script>
     $('#AddonEdit').modal('show');
 </script>
  @endif



        <script>
            $('.edit').click(function(e){
                var id =$(this).attr('data-id');
                console.log(`{{ url('/addon/${id}/edit')}}`);
                $.get(`{{ url('/addon/${id}/edit')}}`)
                .done(function(data){
                    console.log(data);
                    // open edit modal 
                    $('#AddonEdit').modal('show');
                    //set variable
                    $('.addon_id').val(data.id);
                    $('.addon_name').val(data.name);
                    $('.addon_price').val(data.price);
                    $('.addon_description').val(data.description);
                    $('.addon_image').attr("src",data.image);
              
                    // update post
                })
                
            })

            $('.delete').click(function(e){
                var id =$(this).attr('data-id');
                var result = window.confirm('are you sure want to delete');
                if(result == true){
                    $.get(`{{url('/addon/${id}/delete')}}`)
                    .done(function(e){
                        location.reload();
                    })
                }
            })


            //dissapier alert
            $('.alert').fadeIn().delay(3000).fadeOut();
        </script>
 @endsection
 @endsection
