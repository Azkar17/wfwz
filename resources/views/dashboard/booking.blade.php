@extends('base.dashboard-layout')
 @section('content')
    <div class="wrapper">
        <!-- Sidebar  -->
        @include('dashboard.sidebar')

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-dark">
                        <i class="fas fa-align-left"></i>
                       
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

     
                </div>
            </nav>

           
            <div class="row inline-flex">
              <div class="col-md-3">  <h2 class="mt-4">booking</h2></div>
              <div class="col-md-3 text-right">
                <label> Filter by package</label>
                <select class="form-control filterpackage">
                  <option>--Select Package--</option>
                @foreach ($package as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-3 text-right">
                <label>search</label>
                <input type="text" class="form-control filterbyname">
              </div>
              <div class="col-md-3"><button class="btn btn-primary  mt-4 text-right" data-toggle="modal" data-target="#CreateBooking">create</button></div>
            </div>
            <div class="row">
              <div class="col-md-6">
                 <label>from</label> 
                 <input type="date" class="form-control from">
                </div>
                <div class="col-md-6">
                  <label>to</label> 
                  <input type="date" class="form-control to">
              </div>
            </div>
            <div class="table-responsive py-2">
              <table class="table table-striped table-sm">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Customer Info</th>
                      <th>Date Time</th>
                      <th>Package</th>
                      <th>Drone</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody class="bookingtable">
                    @forelse ($booking as $item)
                    <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->customer_name}} <br> {{$item->customer_email}} <br> {{$item->customer_contact}}</td>
                    <td>{{$item->booking_date}} <br>
                        {{$item->estimate_end_date}} <br>
                    </td>
                    <td>{{$item->package->name}}</td>
                    <td>{{$item->drone}}</td>
                    <td>{{$item->status}} </td>
                    <td>   <button class="btn  btn-danger btn-sm delete" data-id="{{$item->id}}">Delete </button>
                        <button class="btn  btn-warning btn-sm edit" data-id="{{$item->id}}">Edit</button></td>
                  </tr>
                    @empty
                        
                    @endforelse
                    
                  </tbody>
                  
                </table>
                
                  <div class="float-right">
                    {{$booking->links()}}
                  </div>

                
           
        </div>
    </div>
    </div>


    <div class="modal fade bd-example-modal-lg" id="CreateBooking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Package</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <form method="post" action="{{route('booking.store')}}"  enctype="multipart/form-data">
          @csrf
          <div class="modal-body">
           <div class="row">
              <div class="col-lg-12 px-3">
                  <div class="row my-2">
                      <div class="col-lg ">
                      <label>Customer Name</label>
                      <input type="text" name="customer_name"  class="form-control @error('customer_name') is-invalid @enderror">
                      <span>@error('customer_name') {{$message}} @enderror</span>
                    </div>
                    </div>
                    <div class="row my-2">
                      <div class="col-lg">
                      <label>Customer email</label>
                      <input type="text" name="customer_email"  class="form-control @error('customer_email') is-invalid @enderror">
                      <span>@error('customer_email') {{$message}} @enderror</span>
                    </div>
                    </div>
                    <div class="row my-2">
                      <div class="col-lg">
                      <label>Customer Contact</label>
                      <input type="text" name="customer_contact"  class="form-control @error('customer_contact') is-invalid @enderror">
                      <span>@error('customer_contact') {{$message}} @enderror</span>
                    </div>
                    </div>
                  <div class="row my-2">
                      <div class="col-lg">
                      <label>Package</label>
                      <select class="form-control @error('package') is-invalid @enderror" name="package">
                        <option value="">-Select Package </option>
                        @foreach ($package as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                        <span>@error('package') {{$message}} @enderror</span>
                      </select>
                    </div>
                  </div>
                  <div class="row my-2">
                      <div class="col-4-lg px-2">
                      <label>Date</label>
                      <input type="date"  name="date" class="form-control addon_price @error('date') is-invalid @enderror">
                      <span>@error('date') {{$message}} @enderror</span>
                    </div>
                      <div class="col-4-lg px-2">
                      <label>Time</label>
                      <input type="time"  name="time" class="form-control addon_price @error('time') is-invalid @enderror">
                      <span>@error('time') {{$message}} @enderror</span>
                    </div>
                  </div>
                  <div class="row my-2">
                      <div class="form-check">
                        @forelse ($addon as $item)
                        <div class="row ml-2">
                        <input class="form-check-input" type="checkbox" value="{{$item->id}}" name="addon" >
                        <label class="form-check-label" for="defaultCheck1">
                      {{$item->name}}
                        </label>
                      </div>
                        @empty
                          
                        @endforelse 
                      </div>
                      
                  </div>
                  <div class="row my-2">
                      <div class="col-lg">
                      <label>Status</label>
                      <select class="form-control" name="status">
                        <option value="" >-Select Status-</option>
                        <option value="Approve">Approve</option>
                        <option value="Cancel">Cancel</option>
                       
                      </select>
                  </div>
                  </div>
              </div>
           </div>
          </div>
          <div class="modal-footer">
            <button  class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
      </form>
        </div>
      </div>
    </div>
  
    <div class="modal fade bd-example-modal-lg" id="EditBooking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Create Package</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <form method="post" action="{{route('booking.update')}}"  enctype="multipart/form-data">
          @csrf
          <div class="modal-body">
           <div class="row">
              <div class="col-lg-12 px-3">
                <input type="hidden" class="book_id" name="id">
                  <div class="row my-2">
                      <div class="col-lg ">
                      <label>Customer Name</label>
                      <input type="text" name="edit_customer_name"  class="form-control customer_name @error('edit_customer_name') is-invalid @enderror">
                      <span>@error('edit_customer_name') {{$message}} @enderror</span>
                    </div>
                    </div>
                    <div class="row my-2">
                      <div class="col-lg">
                      <label>Customer email</label>
                      <input type="text" name="edit_customer_email"  class="form-control customer_email @error('edit_customer_email') is-invalid @enderror">
                      <span>@error('customer_email') {{$message}} @enderror</span>
                    </div>
                    </div>
                    <div class="row my-2">
                      <div class="col-lg">
                      <label>Customer Contact</label>
                      <input type="text" name="edit_customer_contact"  class="form-control customer_contact @error('edit_customer_contact') is-invalid @enderror">
                      <span>@error('edit_customer_contact') {{$message}} @enderror</span>
                    </div>
                    </div>
                  <div class="row my-2">
                      <div class="col-lg">
                      <label>Package</label>
                      <select class="form-control package @error('edit_package') is-invalid @enderror" name="edit_package">
                        <option>-Select Package </option>
                        @foreach ($package as $item)
                      <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                      </select>
                      <span>@error('edit_package') {{$message}} @enderror</span>
                    </div>
                  </div>
                  <div class="row my-2">
                      <div class="col-4-lg px-2">
                      <label>Date</label>
                      <input type="date"  name="edit_date" class="form-control date @error('edit_date') is-invalid @enderror">
                      <span>@error('edit_date') {{$message}} @enderror</span>
                    </div>
                      <div class="col-4-lg px-2">
                      <label>Time</label>
                      <input type="time"  name="edit_time" class="form-control time @error('edit_time') is-invalid @enderror">
                      <span>@error('edit_time') {{$message}} @enderror</span>
                    </div>
                  </div>
                  <div class="row my-2">
                    <label>Addon</label>
                      <div class="form-check">
                        @forelse ($addon as $item)
                        <div class="row ml-2">
                        <input class="form-check-input" type="checkbox" value="{{$item->id}}" >
                        <label class="form-check-label" for="defaultCheck1">
                          {{$item->name}}
                        </label>
                         </div>
                        @empty
                          
                        @endforelse 
                      </div>
                      
                  </div>
                  <div class="row my-2">
                      <div class="col-lg">
                      <label>Status</label>
                      <select class="form-control" name="status" class="status">
                        <option >-Select Status-</option>
                        <option value="Approve">Approve</option>
                        <option value="Cancel">Cancel</option>
                       
                      </select>
                  </div>
                  </div>
              </div>
           </div>
          </div>
          <div class="modal-footer">
            <button  class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
      </form>
        </div>
      </div>
    </div>
  

    @section('script')
    @if($errors->has('customer_name')||$errors->has('customer_contact')||$errors->has('package')||$errors->has('date')||$errors->has('time'))
    <script>
        $('#CreateBooking').modal('show');
    </script>    
     @endif
    @if($errors->has('edit_customer_name')||$errors->has('edit_customer_contact')||$errors->has('edit_package')||$errors->has('edit_date')||$errors->has('edit_time'))
    <script>
        $('#EditBooking').modal('show');
    </script>    
     @endif
 
    <script>
          
       $('.bookingtable').on('click','.edit',function(e){
                var id =$(this).attr('data-id');
                console.log(`{{ url('/booking/${id}/edit')}}`);
                $.get(`{{ url('/booking/${id}/edit')}}`)
                .done(function(data){
                    console.log(data);
                    // open edit modal 
                    $('#EditBooking').modal('show');
                    //set variable
                    $('.book_id').val(data.id);
                    $('.customer_name').val(data.customer_name);
                    $('.customer_email').val(data.customer_email);
                    $('.customer_contact').val(data.customer_contact);
                    $('.package').val(data.package_id);
                    $('.date').val(data.booking_date);
                    $('.time').val(data.booking_date);
                    $('.status').val(date.status);
              
                    // update post
                })
                
            })

            $('.bookingtable').on('click','.delete',function(e){
                var id =$(this).attr('data-id');
                var result = window.confirm('are you sure want to delete');
                if(result == true){
                    $.get(`{{url('/booking/${id}/delete')}}`)
                    .done(function(e){
                        location.reload();
                    })
                }
            })


            //dissapier alert
            $('.alert').fadeIn().delay(3000).fadeOut();

            
            $('.filterpackage').change(function(e){
              var package=$(this).val();
              // alert(package);
              $.get(`{{route('filter.package')}}`,{package:package})
              .done(function(e){
                console.log(e);
                $('.bookingtable').empty();
                $.each(e,function(key,value){
                  $('.bookingtable').append(`
                  <tr>
                    <td>${value.id}</td>
                    <td>${value.customer_name} <br> ${value.customer_email} <br> ${value.customer_contact}</td>
                    <td>${value.booking_date} <br>
                        ${value.estimate_end_date} <br>
                    </td>
                    <td>${value.package_id}</td>
                    <td></td>
                    <td>${value.status} </td>
                    <td>   <button class="btn  btn-danger btn-sm delete" data-id="${value.id}">Delete </button>
                        <button class="btn  btn-warning btn-sm edit" data-id="${value.id}">Edit</button></td>
                  </tr>
                  `);
                })
              })
            })

            $('.filterbyname').keyup(function(e){
              var value =$(this).val();
              $.get(`{{route('filter.name')}}`,{q:value})
              .done(function(data){
                $('.bookingtable').empty();
                $.each(data,function(key,value){
                  $('.bookingtable').append(`
                  <tr>
                    <td>${value.id}</td>
                    <td>${value.customer_name} <br> ${value.customer_email} <br> ${value.customer_contact}</td>
                    <td>${value.booking_date} <br>
                        ${value.estimate_end_date} <br>
                    </td>
                    <td>${value.package_id}</td>
                    <td></td>
                    <td>${value.status} </td>
                    <td>   <button class="btn  btn-danger btn-sm delete" data-id="${value.id}">Delete </button>
                        <button class="btn  btn-warning btn-sm edit" data-id="${value.id}">Edit</button></td>
                  </tr>
                  `);
                })
              })
            })

            $('.from ,.to').change(function(e){
              // alert($('.from').val());
              var from =$('.from').val();
              var to =$('.to').val();
              if(!from || !to){
                alert(`select date from and to `);
              }else{
                $.get(`{{route('filter.date')}}`,{from:from,to:to})
              .done(function(data){
                $('.bookingtable').empty();
                $.each(data,function(key,value){
                  $('.bookingtable').append(`
                  <tr>
                    <td>${value.id}</td>
                    <td>${value.customer_name} <br> ${value.customer_email} <br> ${value.customer_contact}</td>
                    <td>${value.booking_date} <br>
                        ${value.estimate_end_date} <br>
                    </td>
                    <td>${value.package_id}</td>
                    <td></td>
                    <td>${value.status} </td>
                    <td>   <button class="btn  btn-danger btn-sm delete" data-id="${value.id}">Delete </button>
                        <button class="btn  btn-warning btn-sm edit" data-id="${value.id}">Edit</button></td>
                  </tr>
                  `);
                })
              })
                // alert(` from ${from} to ${to}`);
              }
            })
        </script>
    @endsection
    @endsection


  