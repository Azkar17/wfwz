<nav id="sidebar">
    <div class="sidebar-header">
        <h3>WFWZ</h3>
    </div>
    <ul class="list-unstyled components">

        <li >
        <a href="{{route('booking')}}" @if(request()->routeIs('booking'))
            style="background-color:white ;color:black"
            @endif  >Booking</a>
           
        </li>
   
        <li>
        <a href="{{route('package')}}"  @if(request()->routeIs('package'))
            style="background-color:white ;color:black"
            @endif >Packages</a>
           
        </li>

        <li>
        <a href="{{route('logout')}}">Log out</a>
        </li>
    </ul>
</nav>