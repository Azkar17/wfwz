@extends('base.dashboard-layout')
 @section('content')
<div class="container">

  <div class="row  justify-content-center">
    <div class="col-md-8 mx-auto my-5 mt-5">
      <div class="card">
          <div class="card-header text-center">
            <h3 class="text-center">WFWZ Register</h3>
          </div>
        <form method="POST" action="{{route('register')}}" >
              @csrf
          <div class="card-body">
            <div class="row">
                <div class="col-lg my-2 py-2">
                  <label>
                    Name
                  </label>
                  <input type="text" class="form-control" name="name">
                </div>
            </div>
            <div class="row">
                <div class="col-lg my-2 py-2">
                  <label>
                    Email
                  </label>
                  <input type="email" class="form-control" name="email">
                </div>
            </div>
            <div class="row">
                <div class="col-lg my-2 py-2">
                  <label>
                    Password
                  </label>
                  <input type="password" class="form-control " name="password">
                </div>
            </div>
            <div class="row">
                <div class="col-lg my-2 py-2">
                  <label>
                    Confirm Password
                  </label>
                  <input type="password" class="form-control " name="password_confirmation">
                </div>
            </div>
         
            <div class="row">
                <div class="col-lg my-2 py-2">
                 <button class="btn btn-primary" type="submit">Login</button>
                </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


 @section('script')
     
 @endsection