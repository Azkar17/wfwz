<?php

namespace App\Http\Controllers;

use App\Mail\Done;
use Carbon\Carbon;
use App\Models\Booking;
use App\Models\Package;
use App\Mail\newBooking;
use App\Mail\CancelBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomepageController extends Controller
{
    public function homepage(){
        $package=Package::paginate(5);
        // return view('homepage.home',compact('package'));
        return view('homepage.newtemplate',compact('package'));
    }

    public function homapagebook(Request $request){
        $request->validate([
            'name'=> ['required'],
            'phone'=> ['required'],
            'email'=> ['required'],
            'date'=> ['required'],
            'time'=> ['required'],
            'package'=> ['required']
            ]);
// dd($request->all());
        $booking =new Booking;
        $booking->package_id=$request->package;
        $package =Package::find($request->package);
        $booking->customer_name=$request->name;
        $booking->customer_email=$request->email;
        $booking->customer_contact=$request->phone;
        $booking->booking_date=date('Y-m-d H:i:s', strtotime("$request->date $request->time"));
        $booking->estimate_end_date=Carbon::createFromFormat('Y-m-d H:i:s',  $booking->booking_date)->addMinutes($package->duration)->format('Y-m-d H:i:s');
        $checkbook= Booking::where('package_id', $request->package)->whereDate('booking_date', Carbon::parse($booking->booking_date)->format('Y-m-d'))->get();
        $check  = $this->checkOverlaps($checkbook, Carbon::parse($booking->booking_date)->format('H:i:s'), Carbon::parse($booking->estimate_end_date)->format('H:i:s'));
        if ($request->has('drone')) {$booking->drone='Drone';}
            if ($check == 1) {
            return  redirect()->route('home',$request->package)->withErrors(['clash' => ['Sorry Somebody Already Book Selected Time 😥 Change To Other Time']])->withInput();
            } else {
                $booking->save();
                //send to customer
                Mail::to($request->email)->send(new Done($booking));
                //send to wfwz
                Mail::to('Info@wfwzsports.com.my')->send(new newBooking($booking));
              return redirect()->route('home',$request->package)->with('success','Success Make A Booking For You We Send You an email');
    }
}


    public function checkOverlaps($object, $startTime, $endTime)
    {
        foreach ($object as $item) {
            if ( Carbon::parse($item->booking_date)->format('H:i:s') >= $startTime && Carbon::parse($item->booking_date)->format('H:i:s') <= $endTime || $startTime >= Carbon::parse($item->booking_date)->format('H:i:s') && $startTime <= Carbon::parse($item->estimate_end_date)->format('H:i:s')) {
                return true;
            }
        }
        return false;
    }

    public function listpackage(){
        $package=Package::all();
        return view('homepage.newpackagepage',compact('package'));
    }
    public function singlepackage(Request $request){
        $package=Package::where('rider',1)->get();
        return view('homepage.bookingpackage',compact('request','package'));
    }
    public function doublepackage(Request $request){
        $package=Package::where('rider',2)->get();
        return view('homepage.doublepackage',compact('request','package'));
    }
    public function kapaspackage(Request $request){
        $package=Package::where('rider',3)->get();
        return view('homepage.kapaspackage',compact('request','package'));
    }

    public function azkar(){
        $booking=Booking::first();
        // dd($booking);
        Mail::to('azkarneedforspeed@gmail.com')->send(new Done($booking));
        return 'ok';
    }

    public function cancelBookingPage(Request $request){
        $booking =Booking::findorfail($request->booking);
        return view('homepage.cancelbooking',compact('booking'));
    }
    public function StoreCancelBooking(Request $request){
            $request->validate([
                'booking_id'=>'required'
            ]);
            $booking=Booking::findorfail($request->booking_id);
            $booking->status = 'cancel';
            $booking->save();
            Mail::to('Info@wfwzsports.com.my')->send(new CancelBooking($booking));
            // delete booking
            // send mail to admin 
            //save
            return redirect()->route('home')->with('cancel','Your have successfully cancel Your booking');
    }


}
