<?php

namespace App\Http\Controllers;

use App\Models\Addon;
use Illuminate\Http\Request;

class AddonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $addons =Addon::paginate(15);
        return view('dashboard.addon',compact('addons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>['required','max:255'],
            'description'=>['required','max:255'],
            'price'=>['required']
        ]);
        $addon = new Addon;
        if ($request->hasFile('image')) {
            $destination='public/addon';
            $image=$request->file('image');
            $image_name=$image->getClientOriginalName();
            $path=$request->file('image')->storeAs($destination,$image_name);
            $addon->image=$path;
          }
        $addon->name=$request->name;
        $addon->description=$request->description;
        $addon->price=$request->price;
        $addon->save();
        return redirect()->route('addon')->with('message','Successfully Store Addons');
    }


    public function show(Addon $addon)
    {
        //
    }

    public function edit(Addon $addon)
    {
        return response()->json($addon);   
    }


    public function update(Request $request, Addon $addon)
    {
        $request->validate([
            'edit_name'=>['required','max:255'],
            'edit_description'=>['required','max:255'],
            'edit_price'=>['required']
        ]);
        $addon=Addon::findorfail($request->id);
        if ($request->hasFile('image')) {
            $destination='public/addon';
            $image=$request->file('image');
            $image_name=$image->getClientOriginalName();
            $path=$request->file('image')->storeAs($destination,$image_name);
            $addon->image=$path;
          }
        $addon->name=$request->edit_name;
        $addon->description=$request->edit_description;
        $addon->price=$request->edit_price;
        $addon->save();
        return redirect()->route('addon')->with('message','Successfully Update Addons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Addon  $addon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Addon $addon)
    {
        $addon->delete();
        return response()->json('deleted');
    }
}
