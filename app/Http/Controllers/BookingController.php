<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Addon;
use App\Models\Booking;
use App\Models\Package;
use Illuminate\Http\Request;

class BookingController extends Controller
{

    public function index()
    {
        $package=Package::all();
        $addon=Addon::all();
        $booking=Booking::orderBy('booking_date','ASC')->paginate(15);
        return view('dashboard.booking',compact('package','addon','booking'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'package'=> ['required'],
            'customer_name'=> ['required'],
            'customer_contact'=> ['required'],
            'date'=> ['required'],
            'time'=> ['required']
            ]);
        $booking =new Booking;
        $package =Package::find($request->package);
        $booking->package_id=$request->package;
        $booking->customer_name=$request->customer_name;
        $booking->customer_email=$request->customer_email;
        $booking->customer_contact=$request->customer_contact;
        $booking->booking_date=date('Y-m-d H:i:s', strtotime("$request->date $request->time"));
        $booking->estimate_end_date=Carbon::createFromFormat('Y-m-d H:i:s',  $booking->booking_date)->addMinutes($package->duration);
        $booking->addons=$request->addon;
        $booking->status=$request->status;
        $booking->save();
        return redirect()->route('booking');
    }


    public function show(Booking $booking)
    {
        //
    }


    public function edit(Booking $booking)
    {
        return response()->json($booking);
    }

 
    public function update(Request $request)
    {

        $request->validate([
            'id'=> ['required'],
            'edit_package'=> ['required'],
            'edit_customer_name'=> ['required'],
            'edit_customer_contact'=> ['required'],
            'edit_date'=> ['required'],
            'edit_time'=> ['required']
            ]);
        $booking =Booking::findorfail($request->id);
        $booking->package_id=$request->edit_package;
        $package =Package::find($request->edit_package);
        $booking->customer_name=$request->edit_customer_name;
        $booking->customer_email=$request->edit_customer_email;
        $booking->customer_contact=$request->edit_customer_contact;
        $booking->booking_date=date('Y-m-d H:i:s', strtotime("$request->edit_date $request->edit_time"));
        $booking->estimate_end_date=Carbon::createFromFormat('Y-m-d H:i:s',  $booking->booking_date)->addMinutes($package->duration);
        $booking->addons=$request->addon;
        $booking->status=$request->status;
        $booking->save();
        return redirect()->route('booking');
    }

 
    public function destroy(Booking $booking)
    {
        $booking->delete();
        return response()->json('deleted');
    }
    public function filterbypackage(Request $request){
        $booking = Booking::with('addon','package')->where('package_id',$request->package)->get();
        return response()->json($booking);
    }
    public function filterbyname(Request $request){
        $booking = Booking::with('addon','package')->where('customer_name',$request->q)
            ->orWhere('customer_email',$request->q)
            ->orWhere('booking_date','LIKE','%'.$request->q.'%')
            ->orWhere('customer_contact',$request->q)
            ->get();
        return response()->json($booking);
    }
    public function filterdate(Request $request){
        $booking = Booking::with('addon','package')
        ->whereDate('booking_date','>=',$request->from)
        ->whereDate('booking_date','<=',$request->to)
        ->get();
        return response()->json($booking);
    }

}

