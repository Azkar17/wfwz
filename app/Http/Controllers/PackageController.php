<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PackageController extends Controller
{
 
    public function index()
    {
        $packages=Package::paginate(15);
        return view('dashboard.package',compact('packages'));
    }


    public function create()
    {
        
    }


    public function store(Request $request)
    {
    
        $request->validate([
                'name'=> ['required', 'max:255'],
                'description'=> ['required'],
                'price'=> ['required'],
                'duration'=> ['required'],
                'rider'=> ['required'],
                    ]);
        $package =new Package;
        if ($request->hasFile('image')) {
          $destination='public/package';
          $image=$request->file('image');
          $image_name=$image->getClientOriginalName();
          $path=$request->file('image')->storeAs($destination,$image_name);
          $package->image=$path;
        }
        $package->name=$request->name;
        $package->description=$request->description;
        $package->price=$request->price;
        $package->rider=$request->rider;
        $package->duration=$request->duration;
        $package->save();
        return redirect()->route('package')->with('message','Berjaya di simpan');
    }


    public function show(Package $package)
    {
        //
    }

 
    public function edit(Package $package)
    {
        return response()->json($package);
    }

    public function update(Request $request)
    {  $request->validate([
        'edit_name'=> ['required', 'max:255'],
        'edit_description'=> ['required'],
        'edit_price'=> ['required'],
        'edit_rider'=> ['required'],
        'edit_duration'=> ['required'],
            ]);
            $package=Package::findorfail($request->id);
                if ($request->hasFile('image')) {
                    $destination='public/package';
                    $image=$request->file('image');
                    $image_name=$image->getClientOriginalName();
                    $path=$request->file('image')->storeAs($destination,$image_name);
                    $package->image=$path;
                  }
            $package->name=$request->edit_name;
            $package->description=$request->edit_description;
            $package->price=$request->edit_price;
            $package->rider=$request->edit_rider;
            $package->duration=$request->edit_duration;
            $package->save();
            return redirect()->route('package')->with('message','Berjaya di Update');
        
    }

  
    public function destroy(Package $package)
    {
        $package->delete();
        return response()->json('deleted');
    }


    public function getpackage(Package $package){
        return response()->json($package);
    }

    
}
