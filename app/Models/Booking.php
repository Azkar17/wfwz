<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;


    public function package(){
        return $this->belongsTo('App\Models\Package');
    }
    public function addon(){
        return $this->belongsTo('App\Models\Addon','addons','id');
    }
    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('storage/' . str_replace('public/', '', $value));
        } else {
            return 'https://dummyimage.com/250/ffffff/000000';
        }
        
    }
}
