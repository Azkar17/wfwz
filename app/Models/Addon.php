<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Addon extends Model
{
    use HasFactory;
    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('storage/' . str_replace('public/', '', $value));
        } else {
            return 'https://dummyimage.com/250/ffffff/000000';
        }
        
    }
}
