<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Package extends Model
{
    use HasFactory ,SoftDeletes;

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('storage/' . str_replace('public/', '', $value));
        } else {
            return 'https://dummyimage.com/250/ffffff/000000';
        }
        
    }
}
