<?php

namespace App\Mail;

use App\Models\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CancelBooking extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $booking;
    public function __construct(Booking $booking)
    {
        $this->booking=$booking;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $booking=$this->booking;
        return $this->from('donotreply@wfwzsport.com')
                    ->subject('Somebody Cancel Their Booking')
                    ->markdown('emails.cancelbooking',compact('booking'));
    }
}
